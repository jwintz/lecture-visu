#!/usr/bin/env bash

# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

BUILD_CONFIG=Release

cmake -DCMAKE_INSTALL_PREFIX="${PREFIX}" .

make -j${CPU_COUNT}
make install

#
# build.sh ends here
