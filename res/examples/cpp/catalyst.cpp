// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    std::string cpPythonFile = argv[1];

    int nSteps = atoi(argv[2]);

    vtkCPProcessor* processor = vtkCPProcessor::New();
    processor->Initialize();

    vtkCPPythonScriptPipeline* pipeline = vtkCPPythonScriptPipeline::New();
// read the coprocessing python file

    if(pipeline->Initialize(cpPythonFile.c_str()) == 0) {
        cout << "Problem reading the python script.\n"; return 1;
    }

    processor->AddPipeline(pipeline); pipeline->Delete();

    if (nSteps == 0) {
        return 0;
    }

    double tStart = 0.0;
    double tEnd = 1.0;
    double stepSize = (tEnd - tStart)/nSteps;

    vtkCPDataDescription* dataDesc = vtkCPDataDescription::New(); dataDesc->AddInput("input");

    for (int i = 0; i < nSteps; ++i) {
        double currentTime = tStart + stepSize*i; // set the current time and time step dataDesc->SetTimeData(currentTime, i);
// check if the script says we should do coprocessing now
        if(processor->RequestDataDescription(dataDesc) != 0) {
// create our vtkDataObject with the grids and fields
            vtkDataObject* dataObject = <generate grid>;
            dataDesc->GetInputDescriptionByName("input")->SetGrid(dataObject); processor->CoProcess(dataDesc);
        }
    }

    dataDesc->Delete(); processor->Finalize(); processor->Delete();

    MPI_Finalize(); return 0;
}

//
// catalyst.cpp ends here
