// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.Actor {
            property.edgeVisibility: true;

            Vtk.PolyDataMapper {

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

                Vtk.ElevationFilter {

                    id: filter;

                    Vtk.SphereSource {
                        phiResolution: 32;
                        thetaResolution: 32;
                    }

                    onCompleted: {
                        console.log("Completed");
                        console.log(lowPoint);

                         lowPoint.x = 0.0;  lowPoint.y = 0.0;  lowPoint.z = -1.0;
                        highPoint.x = 0.0; highPoint.y = 0.0; highPoint.z = +1.0;
                    }
                }
// /////////////////////////////////////////////////////////////////////////////
            }
        }
    }
}

//
// ElevatedSphere.qml ends here
