// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.Actor {

            property.edgeVisibility: true;

            Vtk.PolyDataMapper {

                Vtk.ShrinkPolyData {

                    shrinkFactor: 0.5;

                    Vtk.ElevationFilter {

                        Vtk.SphereSource {
                            phiResolution: 12;
                            thetaResolution: 12;
                        }

                        onCompleted: {
                             lowPoint.x = 0.0;  lowPoint.y = 0.0;  lowPoint.z = -0.5;
                            highPoint.x = 0.0; highPoint.y = 0.0; highPoint.z = +0.5;
                        }
                    }
                }
            }
        }
    }
}

//
// ElevatedSphere.qml ends here
