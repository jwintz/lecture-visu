// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.Actor {
            property.edgeVisibility: true;

            Vtk.PolyDataMapper {
                Vtk.SphereSource {
                    phiResolution: 8;
                    thetaResolution: 8;
                }
            }
        }

        Vtk.Actor {
            property.edgeVisibility: true;

            Vtk.PolyDataMapper {

                Vtk.Glyph3D {

                    id: filter;

                    vectorMode: Vtk.Glyph3D.UseNormal;
                    scaleMode: Vtk.Glyph3D.ScaleByVector;
                    scaleFactor: 0.12;
                   
                    Vtk.SphereSource {
                        phiResolution: 8;
                        thetaResolution: 8;
                    }

                    Vtk.ConeSource {
                        resolution: 6;
                    }
                }
            }
        }
    }
}

//
// ElevatedSphere.qml ends here
