// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.Actor {
            property.edgeVisibility: true;

            Vtk.PolyDataMapper {

// /////////////////////////////////////////////////////////////////////////////
// Data type
// /////////////////////////////////////////////////////////////////////////////

                Vtk.PolyData {
                    id: source;

                    onCompleted: {
                        var p1 = source.addPoint(-1, -1, 0);
                        var p2 = source.addPoint(+1, -1, 0);
                        var p3 = source.addPoint(+1, +1, 0);
                        var p4 = source.addPoint(-1, +1, 0);

                        var t1 = source.addTriangle(p1, p2, p3);

                        console.log("Point1 id: " + p1);
                        console.log("Point1 id: " + p2);
                        console.log("Point1 id: " + p3);
                        console.log("Triangle1 id: " + t1);
                    }
                }
// /////////////////////////////////////////////////////////////////////////////
            }

        }

        Vtk.Actor {
            id: actor;

            Vtk.PolyDataMapper {
                Vtk.VertexGlyphFilter {
// /////////////////////////////////////////////////////////////////////////////
// X-ref for data type using new properties
// /////////////////////////////////////////////////////////////////////////////
                    inputData: source;
// /////////////////////////////////////////////////////////////////////////////
                }
            }
        }
    }

    Utils.View {
        title: "Property"

        Utils.Slider { from: actor.property; bind: "opacity"; min: 0; max: 1; step: 0.01; value: 1; }
        Utils.Slider { from: actor.property; bind: "pointSize"; min: 1; max: 15; step: 0.1; value: 2; }
        Utils.Slider { from: actor.property; bind: "ambient"; min: 0; max: 1; step: 0.01; value: 0.1; }
        Utils.Slider { from: actor.property; bind: "diffuse"; min: 0; max: 1; step: 0.01; value: 0.75; }
        Utils.ColorPicker { from: actor.property; bind: "ambientColor"; color: "#fff" }
        Utils.ColorPicker { from: actor.property; bind: "diffuseColor"; color: "#fff" }
    }
}

//
// PolyData.qml ends here
