// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12
import QtQuick.Layouts 1.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;


    Vtk.Viewer {
        anchors.fill: parent;
        mouseEnabled: true;

        Vtk.Actor {

            id: actor;

            Vtk.PolyDataMapper {

                Vtk.ContourFilter {

                    id: contour;

                    Vtk.SampleFunction {

                        id: sampler;

                        implicitFunction: Vtk.Quadric {

                            id: quadric;
                        }
                    }
                }
            }
        }
    }

    Utils.View {
        title: "Quadric";

        Utils.Slider {
            from: sampler.sampleDimension; bind: "x"; label: "dimension.x"; min: 1; max: 100; value: 50;
        }

        Utils.Slider {
            from: sampler.sampleDimension; bind: "y"; label: "dimension.y"; min: 1; max: 100; value: 50;
        }

        Utils.Slider {
            from: sampler.sampleDimension; bind: "z"; label: "dimension.z"; min: 1; max: 100; value: 50;
        }
    }

    Component.onCompleted: {
        quadric.setCoefficients(.5,1,.2,0,.1,0,0,.2,0,0);

        contour.generateValues(5,0,1.2);
    }
}

//
// ElevatedSphere.qml ends here
