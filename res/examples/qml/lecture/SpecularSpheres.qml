import QtQuick 2.12

import Vtk 1.0 as Vtk
import Math 1.0 as Math
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.PolyDataMapper {

            id: mapper;

            Vtk.SphereSource {
                id: source;
            }
        }

        Vtk.Actor {
            mapper: mapper;

            position.x: 0.0;
            position.y: 0.0;
            position.z: 0.0;

            property.color: color(255, 0, 0);

        }

/*

 vtkSmartPointer<vtkActor> sphere1 = vtkSmartPointer<vtkActor>::New();
  sphere1->SetMapper(sphereMapper);
  sphere1->GetProperty()->SetColor(1, 0, 0);
  sphere1->GetProperty()->SetAmbient(0.3);
  sphere1->GetProperty()->SetDiffuse(0.0);
  sphere1->GetProperty()->SetSpecular(1.0);
  sphere1->GetProperty()->SetSpecularPower(5.0);

  vtkSmartPointer<vtkActor> sphere2 = vtkSmartPointer<vtkActor>::New();
  sphere2->SetMapper(sphereMapper);
  sphere2->GetProperty()->SetColor(1, 0, 0);
  sphere2->GetProperty()->SetAmbient(0.3);
  sphere2->GetProperty()->SetDiffuse(0.0);
  sphere2->GetProperty()->SetSpecular(1.0);
  sphere2->GetProperty()->SetSpecularPower(10.0);
  sphere2->AddPosition(1.25, 0, 0);

  vtkSmartPointer<vtkActor> sphere3 = vtkSmartPointer<vtkActor>::New();
  sphere3->SetMapper(sphereMapper);
  sphere3->GetProperty()->SetColor(1, 0, 0);
  sphere3->GetProperty()->SetAmbient(0.3);
  sphere3->GetProperty()->SetDiffuse(0.0);
  sphere3->GetProperty()->SetSpecular(1.0);
  sphere3->GetProperty()->SetSpecularPower(20.0);
  sphere3->AddPosition(2.5, 0, 0);

  vtkSmartPointer<vtkActor> sphere4 = vtkSmartPointer<vtkActor>::New();
  sphere4->SetMapper(sphereMapper);
  sphere4->GetProperty()->SetColor(1, 0, 0);
  sphere4->GetProperty()->SetAmbient(0.3);
  sphere4->GetProperty()->SetDiffuse(0.0);
  sphere4->GetProperty()->SetSpecular(1.0);
  sphere4->GetProperty()->SetSpecularPower(40.0);
  sphere4->AddPosition(3.75, 0, 0);

  vtkSmartPointer<vtkActor> sphere5 = vtkSmartPointer<vtkActor>::New();
  sphere5->SetMapper(sphereMapper);
  sphere5->GetProperty()->SetColor(1, 0, 0);
  sphere5->GetProperty()->SetAmbient(0.3);
  sphere5->GetProperty()->SetDiffuse(0.0);
  sphere5->GetProperty()->SetSpecular(0.5);
  sphere5->GetProperty()->SetSpecularPower(5.0);
  sphere5->AddPosition(0.0, 1.25, 0);

  vtkSmartPointer<vtkActor> sphere6 = vtkSmartPointer<vtkActor>::New();
  sphere6->SetMapper(sphereMapper);
  sphere6->GetProperty()->SetColor(1, 0, 0);
  sphere6->GetProperty()->SetAmbient(0.3);
  sphere6->GetProperty()->SetDiffuse(0.0);
  sphere6->GetProperty()->SetSpecular(0.5);
  sphere6->GetProperty()->SetSpecularPower(10.0);
  sphere6->AddPosition(1.25, 1.25, 0);

  vtkSmartPointer<vtkActor> sphere7 = vtkSmartPointer<vtkActor>::New();
  sphere7->SetMapper(sphereMapper);
  sphere7->GetProperty()->SetColor(1, 0, 0);
  sphere7->GetProperty()->SetAmbient(0.3);
  sphere7->GetProperty()->SetDiffuse(0.0);
  sphere7->GetProperty()->SetSpecular(0.5);
  sphere7->GetProperty()->SetSpecularPower(20.0);
  sphere7->AddPosition(2.5, 1.25, 0);

  vtkSmartPointer<vtkActor> sphere8 = vtkSmartPointer<vtkActor>::New();
  sphere8->SetMapper(sphereMapper);
  sphere8->GetProperty()->SetColor(1, 0, 0);
  sphere8->GetProperty()->SetAmbient(0.3);
  sphere8->GetProperty()->SetDiffuse(0.0);
  sphere8->GetProperty()->SetSpecular(0.5);
  sphere8->GetProperty()->SetSpecularPower(40.0);
  sphere8->AddPosition(3.75, 1.25, 0);


  */

    }

    Utils.View {
        title: "SphereSource"

        Utils.Slider {
            from: source; bind: "radius";
            min: 0.1; max: 1; step: 0.01; value: 0.5;
        }

        Utils.Slider {
            from: source; bind: "startTheta";
            min: 0; max: 90; step: 1; value: 0;
        }

        Utils.Slider {
            from: source; bind: "endTheta";
            min: 180; max: 360; step: 1; value: 360;
        }

        Utils.Slider {
            from: source; bind: "startPhi";
            min: 0; max: 90; step: 1; value: 0;
        }

        Utils.Slider {
            from: source; bind: "endPhi";
            min: 90; max: 180; step: 1; value: 180;
        }

        Utils.Slider {
            from: source; bind: "thetaResolution";
            min: 3; max: 64; step: 1; value: 16;
        }

        Utils.Slider {
            from: source; bind: "phiResolution";
            min: 3; max: 64; step: 1; value: 16;
        }

        Utils.CheckBox {
            from: source; bind: "latLongTessellation";
        }
    }
}
