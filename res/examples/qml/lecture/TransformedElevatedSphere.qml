// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

import Vtk 1.0 as Vtk
import Utils 1.0 as Utils

Item {
    id: root;

    anchors.fill: parent;

    Vtk.Viewer {
        anchors.fill: parent;

        mouseEnabled: true;

        Vtk.Actor {
            property.edgeVisibility: true;

            Vtk.PolyDataMapper {
                Vtk.ElevationFilter {
// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

                    Vtk.TransformFilter {

                        id: transformator;

                        Vtk.SphereSource {
                            phiResolution: 32;
                            thetaResolution: 32;
                        }
                    }
                }
// /////////////////////////////////////////////////////////////////////////////
            }
        }
    }

    Utils.View {
        title: "Transform"

        Utils.Slider {
            from: transformator.scale; bind: "x"; label: "scale.x";
            min: 1; max: 10; step: 0.01; value: 1;
        }
        Utils.Slider {
            from: transformator.scale; bind: "y"; label: "scale.y";
            min: 1; max: 10; step: 0.01; value: 1.5;
        }
        Utils.Slider {
            from: transformator.scale; bind: "z"; label: "scale.x";
            min: 1; max: 10; step: 0.01; value: 2.0;
        }
    }

}

//
// ElevatedSphere.qml ends here
