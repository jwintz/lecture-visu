import QtQuick 2.3

Rectangle {
    id: clock

    anchors.centerIn: parent

    width: parent.width / 2
    height: width
    radius: width / 2

    border.color: "#48f"
    border.width: 4

    property int second: 0
    property int minute: 0

    function timeChanged () {
        ++clock.second;

        if (clock.second == 60) {
            clock.second = 0;
            ++clock.minute;

            if (clock.minute == 60) {
                clock.minute = 0;
            }
        }

        console.log(clock.second)
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        font.bold: true

        text: clock.minute + "m " + clock.second + "s"
        color: "#48f"
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 60;

        spacing: 40;

        Text {
            text: timer.running ? "Stop" : "Start"

            font.bold: true
            color: timer.running ? "#f84" : "#48f"

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    timer.running = !timer.running;
                }
            }
        }

        Text {
            text: "Reset"

            font.bold: true
            color: "#666"

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    clock.minute = 0;
                    clock.second = 0;
                    timer.restart();
                }
            }
        }
    }

    Rectangle {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.horizontalCenter

        width: parent.width / 2 - 12
        height: 10
        radius: height / 2
        color: "#943"

        transform: Rotation {
            origin.x: 5
            origin.y: 5

            angle: clock.second * 6 - 90

            Behavior on angle {
                SpringAnimation {
                    spring: 10
                    damping: 0.3
                    modulus: 360
                }
            }
        }
    }

    Timer {
        id: timer;

        repeat: true
        interval: 1000
        onTriggered: timeChanged()
    }
}
