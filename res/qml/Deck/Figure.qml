// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12

Rectangle {

    property alias figure: image.source;
    property int margin: 6;

    border.color: "#9DA5B4";
    border.width: 6;
    radius: 10;

    clip: true;

    height: parent.height * 0.4;
    width: parent.width * 1.0;

    anchors.bottom: parent.bottom;
    anchors.horizontalCenter: parent.horizontalCenter;

    Image {
        id: image;

        anchors.fill: parent;
        anchors.margins: parent.margin;

        fillMode: Image.PreserveAspectFit;
    }
}

//
// Figure.qml ends here
