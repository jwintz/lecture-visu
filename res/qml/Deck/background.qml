// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.0
import QtQuick.Particles 2.0

import Deck 1.0 as Deck

Item {
    anchors.fill: parent

    ParticleSystem{
        id: particles
    }

    ImageParticle{
        anchors.fill: parent
        system: particles
        source: "particle.png"
        alpha: 0
        colorVariation: 0.3
    }

    Emitter{
        anchors.fill: parent
        system: particles
        emitRate: Math.sqrt(parent.width * parent.height) / 30
        lifeSpan: 2000
        size: 4
        sizeVariation: 2
        acceleration: AngleDirection { angle: 90; angleVariation: 360; magnitude: 20; }
        velocity: AngleDirection { angle: -90; angleVariation: 360; magnitude: 10; }
    }
}

//
// background.qml ends here
