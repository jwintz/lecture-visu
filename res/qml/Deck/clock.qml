// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.0

import UI 1.0 as UI

Rectangle {

    id: clock_item;

    property color textColor: root.textColor;
    property color footerColor: root.footerColor;

    property string fontFamily: root.fontFamily;

    color: footerColor;

    anchors.bottom: parent.bottom;
    anchors.right: parent.right;

    height: 34;
    width: header_r_clock.width + clock.width;

    UI.Icon {

        id: header_r_clock;

        anchors.right: parent.right;
        anchors.rightMargin: clock.width + 24;
        anchors.bottom: parent.bottom;
        // anchors.bottomMargin: parent.bottomMargin;

        height: 34;

        icon: icons.fa_clock_o;
        color: "#9DA5B4";
    }

    Text {
        id: clock

        text: currentTime();

        function currentTime() {
            var d = new Date();
            var m = d.getMinutes();
            if (m < 10) m = "0" + m;
            return d.getHours() + ":" + m;
        }

        color: textColor;

        anchors.right: parent.right;
        anchors.rightMargin: 18;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 9;
       
        Timer {
            interval: 60000;
            repeat: true;
            running: true
            onTriggered: clock.text = clock.currentTime();
        }
    }

    Rectangle {
        id: clock_separator;

        color: "#181A1F";

        anchors.left: header_r_clock.left;
        anchors.leftMargin: -12;
        anchors.bottom: parent.bottom;

        width: 1;
        height: 34;

        visible: slides[currentSlide].section.length;
    }
}

//
// clock.qml ends here
