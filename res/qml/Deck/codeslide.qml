// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


import QtQuick 2.0

import App  1.0 as App

Slide  {
    focus: true;

    property alias source: editor.text;

    Rectangle {
        anchors.fill: parent;
        clip: true;
        color: "#282C34";
        border.width: 2;
        border.color: "#aaaaaa";
        radius: 4;

        App.Editor {

            id: editor;

            anchors.fill: parent;
        }
    }
}


//
// codeslide.qml ends here
