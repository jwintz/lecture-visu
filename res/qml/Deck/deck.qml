// -*- mode: QML; tab-width: 4; -*-

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12
import QtQuick.Window 2.2

import App  1.0 as App
import Deck 1.0 as Deck
import UI 1.0 as UI

Window {
    id: window;

    color: "#21252B";

    App.Showcase {
        id: showcase;

        anchors.fill: parent;

        visible: false;
    }

    Deck.Presentation {
        id: presentation;

        mouseNavigation: false;

        anchors.fill: parent;

        titleColor: "#cccccc";
        textColor: "#cccccc";

        footerColor: "#282C34";

        fontFamily: "Roboto";

        Deck.Background {}

        Slide  {
            centeredText: "Scientific visualisation";
        }

        Slide  {
            title: "About me";

            centeredText: "<code>julien.wintz@inria.fr</code>"
        }

        Slide {
            title: "About this lecture";

            section: "";
            subsection: "";
            subsubsection: "";

            content: [
                "A self contained repository for scientific visualisation",
                "A self contained repository for vtk evaluation",
                "A self contained repository for example|test datasets",
                "A self contained repository for project skeleton",
            ]
        }

        Slide {
            title: "About this lecture";

            section: "";
            subsection: "";
            subsubsection: "";

            content: [
                "<code>conda create -n lecture-visu</code>",
                "<code>conda activate lecture-visu</code>",
                "<code>conda install -c dtk-forge-staging -c conda-forge vtk</code>",
                "Clone it at: <code>https://gitlab.inria.fr/jwintz/lecture-visu.git</code>",
            ]
        }

        Slide {
            title: "About this lecture";

            section: "";
            subsection: "";
            subsubsection: "";

            content: [
                "<code>cd lecture</code>",
                "<code>mkdir build; cd build</code>",
                "<code>cmake ..</code>",
                "<code>make</code>",
            ]
        }

        Slide {
            title: "About this lecture";

            section: "";
            subsection: "";
            subsubsection: "";

            content: [
                "Look for examples in QML",
                "Look for examples in C++ as counterparts",
                "Look for Hands on Sessions using Paraview",
                "Assets are in <code>/data/</code>",
                "Samples are in <code>/samples/</code>"
            ]
        }

        Slide    {} // Showcase
        Slide    {} // Showcase

        Slide {
            title: "Outline";

            section: "";
            subsection: "";
            subsubsection: "";

            content: [
                "1 - Foundations",
                "2 - Domain representation",
                "3 - Scalar field rendering",
                "4 - Vector|Tensor field rendering",
                "5 - In situ & Parallel rendering"
            ]
        }

        // /////////////////////////////////////////////////////////////////////////////
        // 1 - Foundations
        // /////////////////////////////////////////////////////////////////////////////

        Slide    {
            title: "Foundations";

            section: "1 - Foundations";
            subsection: "";
            subsubsection: "";

            content: [
                "1.1 - Rendering in theory",
                "1.2 - Rendering in VTK: the pipeline"
            ]
        }

        Slide    {
            title: "Rendering";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "";

            content: [
                "1.1.1 - Overview",
                "1.1.2 - Methods",
                "1.1.3 - Lights",
                "1.1.4 - Materials",
                "1.1.5 - Camera",
                "1.1.6 - Coordinate systems",
                "1.1.7 - Coordinate transformations",
            ]
        }

        Slide    {
            title: "Overview";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.1 - Overview";

            content: [
                "Physical generation of an image:",
                " Rays of light are emitted from a light source in many directions",
                " Some of these rays strike object of interest",
                "  They absorb some of the incident light and reflect the rest of it",
                "  Some of this reflected light may head towards us",
            ]

            Deck.Figure {
                figure: "Figure3-1";
            }
           
        }

        Slide    {
            title: "Overview";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.1 - Overview";

            content: [
                "Raytracing simulates the interaction of light with objects by following the path of each light ray",
                "We follow rays backwards from the viewer's eyes into determining intersections",
                " If the ray intersects the light, the point is being lit",
                " Otherwise, nothing is lit",
            ]
        }

        Slide    {
            title: "Overview";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.1 - Overview";

            content: [
                "Repeat the process for each light source",
                "Sum the contributions of all light source",
                "Add ambient scatter lightning",
                "Gives lightning for the point",
            ]
        }

        Slide    {
            title: "Methods";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.2 - Methods";

            content: [
                "Image order methods (vs.)",
                "Object order methods",
                "Surface rendering (vs.)",
                "Volume rendering"
            ]
        }

        Slide    {
            title: "Rendering methods";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.2 - Methods";

            content: [
                "Image-order process",
                " Ray tracing",
                " Natural",
                " Slow",
                "Object-order process",
                " Rendering each object, one at a time",
                " Algorithmic (z ordering)",
                " Fast",
            ]
        }

        Slide    {
            title: "Rendering types";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.2 - Methods";

            content: [
                "Surface rendering",
                " Boundaries of objects",
                " Properties: interaction with light",
                " Example: the ground, the cube",
                "Volume rendering",
                " Interior of an object",
                " Properties: opacity",
                " Example: clouds, water, fog",
            ]
        }

        Slide    {
            title: "Lights";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.3 - Lights";

            badges: [
                "vtkLight",
                "vtkLightCollection",
                "vtkLightKit",
            ]

            content: [
                "No light: no image",
                "Types",
                " Point light source",
                "  Emits light from a single point in all directions",
                "  With intensity decreasing with distance",
                " Directional",
                "  Emits uniformly from one direction",
                "  Constant intensity",
            ]
        }

        Slide    {
            title: "Lights";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.3 - Lights";

            badges: [
                "vtkLight",
                "vtkLightCollection",
                "vtkLightKit",
            ]

            content: [
                "Types",
                " Spotlight",
                "  Emits a directed cone of light",
                "  More intense closer to the spotlight source and to the center of the light cone",
                " Ambient",
                "  Illuminate objects even when no other light source is present",
                "  Intensity is <b>independent</b> of direction, distance, and other objects",
            ]
        }

        Slide    {
            title: "Materials";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.4 - Materials";

            badges: [
                "vtkProperty",
            ]

            content: [
                "Ambient properties (indirect lighting, independent of camera position)",
                "Diffuse properties (direct lighting, independent of camera position)",
                "Specular properties (direct lighting, dependent of camera position)",
                "For all them:",
                " Color",
                " Power"
            ]
        }

        Slide    {

        }

        Slide    {
            title: "Camera";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.5 - Camera";

            badges: [
                "vtkCamera",
                "vtkRenderer",
                "vtkRenderWindow",
            ]

            content: [
                "Defines:",
                " Position (of the user)",
                " Orientation (of the user)",
                " Focal (eyes)",
                " Projection type",
                "  Orthographic",
                "  Perspective",
                "Clipping",
            ]
        }

        Slide    {
            title: "Camera";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.5 - Camera";

            badges: [
                "vtkCamera",
                "vtkRenderer",
                "vtkRenderWindow",
            ]
           
            content: [
                "Defines:",
                " Position (of the user)",
                " Orientation (of the user)",
                " Focal (eyes)",
                " Projection type",
                "  Orthographic",
                "  Perspective",
                "Clipping",
            ]

            Figure {
                figure: "Figure3-11";
            }
        }

        Slide    {
            title: "Coordinate systems";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            content: [
                "In computer graphics, 4 of them:",
                " Model coordinate system",
                " World coordinate system",
                " View coordinate system",
                " Display coordinate system",
            ]
        }

        Slide    {
            title: "Model coordinate systems";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            content: [
                "Local cartesian coordinate system",
                "Chosen to be relevant for the model",
                "As many as models",
                "Examples:",
                " A cylindrical system for a sphere",
                " A regular system for a cube",
            ]
        }

        Slide    {
            title: "World coordinate system";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            content: [
                "3D space into which models (actors) are positioned",
                "Converts model coordinates to world coordinates",
                "Only one",
                "Also used for:",
                " Camera",
                " Lights",
            ]
        }

        Slide    {
            title: "View coordinate system";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            content: [
                "Used for the camera",
                "<i>(x, y)</i> in range <i>(-1, 1)</i>",
                " Position in the image plane",
                "<i>z</i> coordinate for depth",
                " Distance from the camera",
                "One per display",
            ]
        }

        Slide    {
            title: "Display coordinate system";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            badges: [
                "vtkViewport",
                "vtkRenderer",
                "vtkRenderWindow",
            ]

            content: [
                "Same basis the the view one but:",
                "<i>(x, y)</i> are pixel positions in the resulting image",
                "Maps view coordinates WRT/:",
                " Window size",
                " Viewport",
            ]
        }

        Slide    {
            title: "Coordinate systems";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.6 - Coordinate systems";

            Figure {
                figure: "Figure3-14";

                height: parent.height;
            }
        }

        Slide {

        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "In the process, three dimensional coordinates are projected onto a two dimensional image plane",
                "To handle perspective, we need <i>homogeneous coordinates</i>",
                "Setting <i>w</i> to 0 sends the point to the infinite",
            ]

            Figure {
                figure: "homcoords.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "Transformations are applied using a transformations matrix",
                "They include:",
                " Translation",
                " Scaling",
                " Rotation",
            ]

            Figure {
                figure: "t.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "Transformations are applied using a transformations matrix",
                "They include:",
                " Translation",
                " Scaling",
                " Rotation",
            ]

            Figure {
                figure: "s.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "Transformations are applied using a transformations matrix",
                "They include:",
                " Translation",
                " Scaling",
                " Rotation",
            ]

            Figure {
                figure: "rx.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "Transformations are applied using a transformations matrix",
                "They include:",
                " Translation",
                " Scaling",
                " Rotation",
            ]

            Figure {
                figure: "ry.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "Coordinate transformations";

            section: "1 - Foundations";
            subsection: "1.1 - Rendering";
            subsubsection: "1.1.7 - Coordinate transformations";

            badges: [
                "vtkMath",
                "vtkVector*",
                "vtkMatrix*",
                "vtkMatrix4x4",
                "vtkTransform",
            ]

            content: [
                "Transformations are applied using a transformations matrix",
                "They include:",
                " Translation",
                " Scaling",
                " Rotation",
            ]

            Figure {
                figure: "rz.png";
                color: "#282C34";
                margin: 20;
            }
        }
       
        // /////////////////////////////////////////////////////////////////////////////

        Slide    {
            title: "Pipeline";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "";

            content: [
                "1.2.1 - Overview",
                "1.2.2 - Objects",
                "1.2.3 - Execution",
                "1.2.4 - Examples",
            ]
        }

        Slide    {
            title: "Overview";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            badges: [
                "vtkAlgorithm",
                "vtkDataObject",
                "vtkDemandDrivenPipeline",
                "vtkExecutive",
            ]

            content: [
                "How transformations and representations are organised into an effective rendering algorithm",
                "Transformation:",
                " Converts data from its original form into graphic primitives",
                " Processes in a functional model",
                "Representation:",
                " Displays graphic primitives",
                " Objects in an object model",
            ]
        }

        Slide    {
            title: "A data visualisation example";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            content: [
                "A data visualisation example:"
            ]

            Figure {
                figure: "quadric.png";
                color: "#282C34";
                margin: 20;
            }
        }

        Slide    {
            title: "A data visualisation example";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            badges: [
                "vtkSampleFunction",
                "vtkQuadric",
                "vtkCutter",
                "vtkContour",
                "vtkPolyDataMapper",
                "vtkActor",
            ]

            content: [
                "A data visualisation example:",
                " Sampled on a regular grid",
                " Surface extraction",
                " Cut planes extraction",
                " Curve extraction with the planes"
            ]
        }

        Slide    {
            title: "The functional model";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            content: [
                "Rect blocks: data",
                "Oval blocks: processes",
                " No input: source",
                " No output: sink",
                " Otherwise: filter",
                "Shows how the data flows through the system"
            ]
        }

        Slide    {
            title: "The functional model";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            Figure {
                figure: "Figure4-1b";

                height: parent.height;
            }
        }

        Slide    {
            title: "The visualisation model";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            Figure {
                figure: "Figure4-1c";

                height: parent.height;
            }
        }

        Slide    {
            title: "The object model";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            content: [
                "Describes which module operate on the dataflow",
                "Options:",
                " Data and processes are combined",
                " Data and processes are distinguished",
                " VTK choice: hybrid",
                "  Strong identification of algorithms",
                "  No duplication",
                "  Some critical operation implemented within objects",
            ]
        }

        Slide    {
            title: "The object model";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.1 - Overview";

            Figure {
                figure: "Figure4-2";

                height: parent.height;
            }
        }

        Slide    {
            title: "Objects";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.2 - Objects";

            content: [
                "Data objects",
                "Process objects",
                "Connections",
            ]
        }

        Slide    {
            title: "Data Objects";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.2 - Objects";

            badges: [
                "vtkDataObject",
            ]

            content: [
                "Represent information",
                "Convenience methods for:",
                " Creation",
                " Access",
                " Deletion",
                "No direct modification"
            ]
        }

        Slide    {
            title: "Process Objects";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.2 - Objects";

            badges: [
                "vtkAlgorithm",
            ]

            content: [
                "Operate on input data to produce output data",
                "Either:",
                " Derives new data from input",
                " Transforms input data",
            ]
        }

        Slide    {
            title: "Process Objects";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.2 - Objects";

            badges: [
                "vtkAlgorithm",
            ]

            content: [
                "Types:",
                " Source process objects",
                "  Interface to external data sources (readers)",
                "  Generate data sources (procedural ones)",
                " Filter process objects",
                "  One or more inputs data objects",
                "  Generate one or more output data objects",
                " Sink|Mapper process objects",
                "  One or more inputs data objects",
                "  Convert data into graphical primitives",
                "  End the workflow",
            ]
        }

        Slide    {
            title: "Connections";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.2 - Objects";

            content: [
                "Arrows in the models",
                "Type:",
                " What kind is the input",
                "Multiplicity:",
                " How many inputs",
            ]

            Figure {
                figure: "Figure4-4";
            }
        }

        Slide    {
            title: "Execution";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.3 - Execution";

            content: [
                "Data, Processes and Connections form a pipeline",
                "Executed to achieve rendering",
                "Usually more than once",
                "Requires synchronization:",
                " Implicit control",
                " Explicit control",
            ]
        }

        Slide    {
            title: "Execution";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.3 - Execution";

            content: [
                "Data, Processes and Connections form a pipeline",
                "Executed to achieve rendering",
                "Usually more than once",
                "Requires synchronization:",
                " Implicit control",
                " Explicit control",
            ]

            Figure {
                figure: "Figure4-6";

                height: parent.height * 0.6;
            }
        }

        Slide    {
            title: "Explicit execution";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.3 - Execution";

            content: [
                "Direct changes to the network",
                "Explicit dependency analysis",
                "Uses an executive for coordination",
                "Pros:",
                " Synchronization analysis is localised",
                "Cons:",
                " Each process is dependent on the executive",
                "Can be event-driven or demand-driven",
            ]
        }

        Slide    {
            title: "Implicit execution";

            section: "1 - Foundations";
            subsection: "1.2 - Pipeline";
            subsubsection: "1.2.3 - Execution";

            content: [
                "Process executes only if its local input or parameter change",
                "Two passes:",
                " Output is requested from a process. It then requests data from its input and so on backwards",
                " Output is executed forwards",
                "Pros:",
                " Simpler, uses modification timestamps",
                "Cons:",
                " Harder to distribute amongst multiple processors",
            ]
        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "1 - Foundations";
            subsection: "VTK";
            subsubsection: "Code samples";

            source: Helper.read("/res/examples/cpp/hello.cpp");
        }

        Slide {

        }

        Slide {

        }

        Slide {

        }

        Slide {

        }

        Slide {

        }

        // /////////////////////////////////////////////////////////////////////////////
        // 2 - Domain representation
        // /////////////////////////////////////////////////////////////////////////////

        Slide   {
            title: "Domain representation"

            section: "2 - Domain representation";
            subsection: "";
            subsubsection: "";

            content: [
                "2.1 - Overview",
                "2.2 - Cells",
                "2.3 - Attributes",
                "2.4 - Datasets",
            ]
        }

        Slide   {
            title: "Characterisation"

            section: "2 - Domain representation";
            subsection: "2.1 - Overview";
            subsubsection: "";

            content: [
                "Characterising data",
                " Discrete or continuous ?",
                " Regular or irregular ?",
                " Topological dimension ?"
            ]
        }

        Slide   {
            title: "Characterisation"

            section: "2 - Domain representation";
            subsection: "2.1 - Overview";
            subsubsection: "";

            content: [
                "Characterising data",
                " Discrete or continuous ?",
                "  If continuous then discretise",
                " Regular or irregular ?",
                "  <i>Aka structured or unstructured</i>",
                " Topological dimension ?"
            ]
        }

        Slide   {
            title: "Design criteria"

            section: "2 - Domain representation";
            subsection: "2.1 - Overview";
            subsubsection: "";

            content: [
                "Compact: minimise memory requirements",
                "Efficient: minimise processor requirements",
                "Mappable:",
                " Into graphics primitives",
                " Into efficient data structures",
                "Minimal coverage: a well defined semantic",
                "Simple!!!"
            ]
        }

        Slide   {
            title: "Summary"

            section: "2 - Domain representation";
            subsection: "2.1 - Overview";
            subsubsection: "";

            badges: [
                "vtkPoints",
            ]

            content: [
                "Geometry: a list of points",
                "Topology: a list of cells",
                "Attributes:",
                " Point centered",
                " Cell centered",
            ]

            Figure {
                figure: "Figure5-1";
            }
        }

        Slide   {
            title: "Cells"

            section: "2 - Domain representation";
            subsection: "2.2 - Cells";
            subsubsection: "";

            badges: [
                "vtkCell",
                "vtkCellArray",
            ]

            content: [
                "Defined by:",
                " A Type",
                " An ordered list of points indices (connectivity)"
            ]

            Figure {
                figure: "Figure5-3";

                height: parent.height * 0.6;
            }
        }

        Slide   {
            title: "Cell types"

            section: "2 - Domain representation";
            subsection: "2.2 - Cells";
            subsubsection: "";

            Figure {
                figure: "Figure5-2";

                height: parent.height;
            }
        }

        Slide   {
            title: "Attributes"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "";

            content: [
                "2.3.1 - Scalars",
                "2.3.2 - Vectors",
                "2.3.3 - Normals",
                "2.3.4 - Tensors",
                "2.3.5 - Texture coordinates"
            ]
        }

        Slide   {
            title: "Attributes"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "";

            badges: [
                "vtkDataArray",
                "vtkCellData",
                "vtkPointData",
                "vtkFieldData",
            ]

            content: [
                "Stored as arrays, they have:",
                " A type <i>e.g.</i>:<code>int</code>",
                " A dimension <i>e.g.</i>:<code>2</code>",
                " A number of components <i>e.g.</i>:<code>3</code>",
            ]
        }


        Slide   {
            title: "Attributes"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "";

            Figure {
                figure: "Figure5-6";

                height: parent.height;
            }
        }

        Slide   {
            title: "Scalars"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "2.3.1 - Scalars";

            badges: [
                "vtkDataArray",
                "vtkCellData",
                "vtkPointData",
                "vtkFieldData",
            ]

            content: [
                "Single valued",
                "Examples:",
                " Temperature",
                " Pressure",
                " Density",
                " Elevation",
            ]
        }

        Slide   {
            title: "Vectors"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "2.3.2 - Vectors";

            badges: [
                "vtkDataArray",
                "vtkCellData",
                "vtkPointData",
                "vtkFieldData",
            ]

            content: [
                "Magnitude and direction",
                "Three dimensional tuple <i>(u, v, w)</i>",
                "Examples:",
                " Velocity",
                " Particle trajectory",
                " Wind motion",
                " Gradient function",
            ]
        }

        Slide   {
            title: "Normals"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "2.3.3 - Normals";

            badges: [
                "vtkDataArray",
                "vtkCellData",
                "vtkPointData",
                "vtkFieldData",
            ]

            content: [
                "Direction",
                "Magnitude = 1",
                "Example usage:",
                " Shading",
                " Cell orientation (CCW around normal)",
                " Orientation of generated data",
            ]
        }

        Slide   {
            title: "Texture coordinates"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "2.3.4 - Texture coordinates";

            badges: [
                "vtkDataArray",
                "vtkCellData",
                "vtkPointData",
                "vtkFieldData",
            ]

            content: [
                "Map a point in cartesian space into:",
                " 1D texture space <i>(u)</i>",
                " 2D texture space <i>(u, v)</i>",
                " 3D texture space <i>(u, v, s)</i>",
            ]
        }

        Slide   {
            title: "Tensors"

            section: "2 - Domain representation";
            subsection: "2.3 - Attributes";
            subsubsection: "2.3.5 - Tensors";

            content: [
                "Generalisation of vectors and matrices",
                "Rank <i>k</i>: <i>k</i>-dimensional table",
                "Rank <i>0</i>: scalar",
                "Rank <i>1</i>: vector",
                "Rank <i>2</i>: matrix",
                "Rank <i>3</i>: 3D rectangular array",
                "..."
            ]
        }

        Slide   {
            title: "Data sets"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "";

            content: [
                "Dataset:",
                " Structure:",
                "  Geometry",
                "  Topology",
                " Attributes"
            ]
        }

        Slide   {
            title: "Data sets"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "";

            content: [
                "2.4.1 - Polygonal data",
                "2.4.2 - Image data",
                "2.4.3 - Rectilinear grids",
                "2.4.4 - Structured grids",
                "2.4.5 - Unstructured grids"
            ]
        }

        Slide   {
            title: "Data sets"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "";

            Figure {
                figure: "Figure5-7";

                height: parent.height;
            }
        }

        Slide   {
            title: "PolyData"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "2.4.1 - PolyData";

            badges: [
                "vtkDataObject",
                "vtkDataSet",
                "vtkPointSet",
                "vtkPolyData",
            ]

            content: [
                "Unstructured geometry and topology",
                "0-dimensional geometry: vertices",
                "1-dimensional geometry: lines (edges)",
                "2-dimensional geometry: polygons (faces)",
                "Boundary only!",
                "Basically a surface mesh",
            ]
        }

        Slide {

        }

        Slide   {
            title: "ImageData"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "2.4.2 - ImageData";

            badges: [
                "vtkDataObject",
                "vtkDataSet",
                "vtkImageData",
            ]

            content: [
                "Structured (regular) geometry and topology",
                "1-dimensional geometry: lines",
                "2-dimensional geometry: pixels",
                "3-dimensional geometry: voxels",
                "Origin",
                "Spacing is fixed",
                "Dimension (major drawback)",
            ]
        }

        Slide {

        }

        Slide   {
            title: "Rectilinear grids"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "2.4.3 - Rectilinear grids";

            badges: [
                "vtkDataObject",
                "vtkDataSet",
                "vtkRectilinearGrid",
            ]

            content: [
                "Structured (regular) topology and partially structured geometry",
                "1-dimensional geometry: lines",
                "2-dimensional geometry: pixels",
                "3-dimensional geometry: voxels",
                "Origin",
                "Spacing may vary",
                "Dimension (major drawback)",
            ]
        }

        Slide   {
            title: "Structured grids"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "2.4.4 - Structured grids";

            badges: [
                "vtkDataObject",
                "vtkDataSet",
                "vtkPointSet",
                "vtkStructuredGrid",
            ]

            content: [
                "Structured (regular) topology and unstructured (irregular) geometry",
                "2D cells: quadrilaterals",
                "3D cells: hexahedrons",
                "Relevant for",
                " FEM",
                " FVM",
                " Fluid flow",
                " Heat transfer",
            ]
        }

        Slide   {
            title: "Unstructured grids"

            section: "2 - Domain representation";
            subsection: "2.4 - Data sets";
            subsubsection: "2.4.5 - Unstructured grids";

            badges: [
                "vtkDataObject",
                "vtkDataSet",
                "vtkPointSet",
                "vtkUnstructuredGrid",
            ]

            content: [
                "Unstructured (irregular) topology and unstructured (irregular) geometry",
                "Most generic dataset",
                "Any cell can be an arbitrary combination",
                "Topology of cells from 0D (vertices) to 3D (tetrahedron, hexahedron, voxel)",
                "Use only when absolutely necessary!",
                " Generic dataset algorithms are less optimised",
            ]
        }

        Slide   {

        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "2 - Domain representation";
            subsection: "VTK";
            subsubsection: "Code samples";

            source: Helper.read("/res/examples/cpp/tetra.cpp");
        }

        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";
        }

        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the dataset <code>data/vectorFields/3-triangulation/output.vtu</code>"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-1.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "To only consider as a pipeline object the actual boundary of the volume:",
                " Apply the <code>Extract Surface</code> filter",
                " Update the pipeline",
                "What shading model is employed?",
                "Apply the necessary filter to have a smooth rendering"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-2.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "Disable the visualization of your active pipeline object",
                "In the Properties panel, adjust the right option to visualize the mesh as a wire-frame on top of the surface"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-3.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "Click on the <code>Select Cells Through</code> button at the top of the visualization panel ('f')",
                "Select some parts of the geometry"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-4.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }
Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "Find and apply the appropriate filter to create a pipeline object that only contains the selected cells",
                "Further apply the necessary filters to obtain a smooth shading of the resulting objects with a wire-frame representation on top of the resulting surface"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-5.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }
        Slide   {
            title: "Hands on session"

            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            content: [
                "Apply the appropriate filters to visualize the boundary surface of our input data-set in smooth shading and modify the appropriate properties to display it in transparency with a distinct color",
                "Also, extract its feature edges and represent them with tubular surfaces"
            ]
        }

        Slide   {
            section: "2 - Domain representation";
            subsection: "2.5 - Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "2-5-6.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        // /////////////////////////////////////////////////////////////////////////////
        // 3 - Scalar field rendering
        // /////////////////////////////////////////////////////////////////////////////

        Slide   {
            title: "Scalar field rendering"

            section: "3 - Scalar field rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "Algorithms transform data",
                "Transformation distinguished by algorithm's:",
                " Structure:",
                "  The effect that the algorithm has on topology and geometry",
                " Type:",
                "  The type of the dataset the algorithm operates on",
            ]
        }

        Slide   {
            title: "Structural transformation"

            section: "3 - Scalar field rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "Geometric transformation: alter input geometry but no topology",
                "Topological transformation: alter input topological but no geometry",
                "Attribute transformation: converts attributes from one form to another",
                "Combined transformation: All or many of them",
            ]
        }

        Slide   {
            title: "Type transformation"

            section: "3 - Scalar field rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "Scalar algorithm: generate scalar fields",
                "Vector algorithm: generate vector fields",
                "Tensors algorithm: generate tensor fields",
                "Modeling algorithm: generate geometry and topology",
                "Examples:",
                " Contour lines (scalar algorithm)",
                " Glyphs (vector algorithm)",
                " Components extraction (tensor algorithm)",
                " Sampling (modeling algorithm)",
            ]
        }

        Slide   {
            title: "Scalar field rendering"

            section: "3 - Scalar field rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "3.1 - Color mapping",
                "3.2 - Level sets",
                "3.3 - Volume rendering",
            ]
        }

        Slide   {
            title: "Color mapping";

            section: "3 - Scalar field rendering";
            subsection: "3.1 - Color mapping";
            subsubsection: "";

            badges: [
                "vtkMapper",
                "vtkDataSetMapper",
                "vtkPolyDataMapper",
            ]

            content: [
                "Map scalar data to colors",
                "Using a lookup table",
                " Scalars are indexes",
                " Colors are values",
                "LUT is a specialisation of a <i>transfer function</i>",
            ]
        }

        Slide   {
            title: "Linear interpolation";

            section: "3 - Scalar field rendering";
            subsection: "3.1 - Color mapping";
            subsubsection: "3.1.1 - Linear interpolation";

            content: [
                "Considering two points: <i>(x0, y0)</i> and <i>(x1, y1)</i>",
                "the linear interpolant is the straight line between these points",
            ]

            Figure {
                figure: "linear_interpolation.png";
                margin: 20;
                color: "#282C34";
            }
        }

        Slide   {
            title: "Linear mapping";

            section: "3 - Scalar field rendering";
            subsection: "3.1 - Color mapping";
            subsubsection: "3.1.2 - Linear mapping";

            Figure {
                figure: "Lookup";

                height: parent.height;
            }
        }

        CodeSlide {
            title: "Color mapping";

            section: "3 - Scalar field rendering";
            subsection: "3.1 - Color mapping";
            subsubsection: "";

            source: Helper.read("/res/examples/cpp/colormap.cpp");
        }

        Slide   {
            title: "Implicit levelsets";

            section: "3 - Scalar field rendering";
            subsection: "3.1 - Color mapping";
            subsubsection: "3.1.3 - Implicit levelsets";

            badges: [
                "vtkMapper",
                "vtkDataSetMapper",
                "vtkPolyDataMapper",
            ]

            content: [
                "Use the LUT as a texture",
                "Draw bars in the LUT",
                " With chosen colors",
                " With chosen width",
                " At relevant indexes (scalar values)",
                "Creates iso values on the color mapping!",
            ]
        }

        Slide   {
            title: "Levelsets";

            section: "3 - Scalar field rendering";
            subsection: "3.2 - Levelsets";
            subsubsection: "";

            badges: [
                "vtkContour",
                "vtkPolyData",
            ]

            content: [
                "Exact geometry visualisation",
                "Regions of interest",
                " Identify",
                " Study",
                " Visualise",
                "  Different layers",
                "  Different level sets",
            ]
        }

        Slide   {
            title: "Levelsets";

            section: "3 - Scalar field rendering";
            subsection: "3.2 - Levelsets";
            subsubsection: "";

            badges: [
                "vtkContour",
                "vtkPolyData",
            ]

            content: [
                "2D: isolines",
                "3D: isosurfaces",
                "Computed using marching squares:",
                " 2D: Marching squares",
                " 3D: Marching cubes",
            ]
        }

        Slide   {
            title: "Generic algorithm";

            section: "3 - Scalar field rendering";
            subsection: "3.2 - Levelsets";
            subsubsection: "";

            content: [
                "Generic algorithm",
                " Select a cell",
                " Calculate the inside/outside state of each vertex of the cell",
                " Create an index by storing the binary state of each vertex in a separate bit",
                " Use the index to look up the topological state of the cell in a case table",
                " Calculate the contour location (via interpolation) for each edge in the case table",
            ]
        }

        Slide   {
            title: "Marching squares sequences";

            section: "3 - Scalar field rendering";
            subsection: "3.2 - Levelsets";
            subsubsection: "3.2.1 - Marching squares";

            Figure {
                figure: "Figure6-5";

                height: parent.height;
            }
        }

        Slide   {
            title: "Marching cubes sequences";

            section: "3 - Scalar field rendering";
            subsection: "3.2 - Levelsets";
            subsubsection: "3.2.1 - Marching cubes";

            Figure {
                figure: "Figure6-6";

                color: "#708090";

                height: parent.height;
            }
        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "3 - Scalar field rendering";
            subsection: "VTK";
            subsubsection: "Code samples";

            source: Helper.read("/res/examples/cpp/contour.cpp");
        }

        Slide   {
            title: "Volume rendering";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]
            
            content: [
                "Volumetric scalar fields:",
                " Acquired datasets: MRI, CT SCAN, ...",
                " Simulated datasets: CFD, pressure, porosity ...",
                "Whole representation",
                "Color transfer function",
                "Opacity transfer function",
                "Rendering:",
                " Accumulation process",
                " View dependent",
            ]
        }

        Slide   {
            title: "Volume rendering";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "";

            content: [
                "3.3.1 - Casting",
                "3.3.2 - Sampling",
                "3.3.3 - Shading",
                "3.3.4 - Compositing",
                "3.3.5 - Blending",
            ]
        }

        Slide   {
            title: "Casting";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.1 - Casting";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]

            content: [
                "For each pixel",
                " Cast a ray",
                " Direction of observation",
                " Intersection problem",
                "  Which cells are traversed by the ray",
                "  Accelerate using octrees",
            ]
        }

        Slide   {
            title: "Casting";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.1 - Casting";

            Figure {
                figure: "casting.png";

                height: parent.height;
            }
        }

        Slide   {
            title: "Sampling";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.2 - Sampling";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]

            content: [
                "Along each ray:",
                " Sample the data along the ray",
                "  Intersection with the edges",
                " Compute the function value on the samples",
                "  Based on the domain interpolant",
            ]
        }

        Slide   {
            title: "Sampling";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.2 - Sampling";

            Figure {
                figure: "sampling.png";

                height: parent.height;
            }
        }

        Slide   {
            title: "Shading";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.3 - Shading";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]

            content: [
                "Light computation for each sample",
                " Retrieve the corresponding color (transfer)",
                " Retrieve the surface normal",
                " Shade the sample accordingly",
                "  Normal",
                "  View direction",
                "  Color",
            ]
        }

        Slide   {
            title: "Shading";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.3 - Shading";

            Figure {
                figure: "sampling.png";

                height: parent.height;
            }
        }

        Slide   {
            title: "Compositing";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.4 - Compositing";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]

            content: [
                "Integrate all the sample's contributions",
                " Along each ray",
                "  From the back to the front",
                "  At each sample",
                "   Retrieve the opacity value (transfer)",
                " Composition along the ray",
                "  Blend the color of the samples",
                "  Based on the opacity of the samples",
            ]
        }

        Slide   {
            title: "Compositing";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.4 - Compositing";

            Figure {
                figure: "compositing.png";

                height: parent.height;
            }
        }

        Slide   {
            title: "Blending";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "3.3.5 - Blending";

            badges: [
                "vtkVolume",
                "vtkVolumeProperty",
                "vtkSmartVolumeMapper",
                "vtkColorTransferFunction",
                "vtkLookupTable",
            ]

            content: [
                "Screen space operation",
                " Blend the colors of the samples",
                " Based on their opacity",
            ]
        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "3 - Scalar field rendering";
            subsection: "3.3 - Volume rendering";
            subsubsection: "C++ Sample";

            source: Helper.read("/res/examples/cpp/volume.cpp");
        }

        Slide {

        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the dataset <code>data/scalarFields/2-grid/output.vti</code>"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-1.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Play with the color map editor to improve the contrast, in particular between the canyon and the rims."
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-2.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Review the actual values of the scalar field on a per vertex basis:",
                " Click on the 'Split Horizontal' button",
                " Choose the spreadsheet view"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-3.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session";
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Find and apply the appropriate filter to generate a surface embedded in 3D such that:",
                " Each point gets an offset",
                " Each offset uses an embedding component equal to its scalar field value"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-4.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the dataset <code>data/scalarFields/3-triangulation/output.vtu</code>",
                "What kind of domain is that?"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-5.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Color maps sometimes fall short for 3D domains with occlusion",
                "Apply the appropriate filters to visualize the boundary of the surface:",
                " Smooth",
                " Transparent",
                "Extract feature edges",
                "Represent them with tubular surfaces"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-6.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "User selection tools enable you to select cells of the input domain",
                "The boundary of the selection appears evenly cut",
                "Find and apply a filter that precisely cut the geometry along the clipping planes",
                "Adjust the color map to yield a better contrast on the cuts of the volume"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-7.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "This visualisation only conveys information about scalar fields along clipping planes",
                "Show the regions of the domain for which vertices take some value in a user defined range:",
                " Specify an interval",
                " Specify an iso value",
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-8.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Display at the same time:",
                " 1 isosurface",
                " 4 isovalues"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-9.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the dataset <code>data/scalarFields/3-grid/output.vti</code>",
                "What kind of domain is that?",
                "We will:",
                " Represent the skin of the foot in red, highly transparent",
                " Represent the boundary of the bones in white, transparent",
                " Represent the inside of the cortical bone in white, almost opaque"
            ]
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "By isosurface extraction, identify:",
                " The isovalue for the skin",
                " The isovalue for the bone",
                " The isovalue for the bone marrow",
                "Color them as expected"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-10.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Delete the isosurfaces",
                "Select the input data set in the pipeline browser",
                "In the properties panel, trigger volume rendering"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-11.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Adjust the transfer function for both color (x) and opacity (y)",
                " For each material, draw a step function",
                " Adjust width and height of these steps",
                " Smooth the steps by inserting more points"
            ]
        }

        Slide   {
            section: "3 - Scalar field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "3-12.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        // /////////////////////////////////////////////////////////////////////////////
        // 4 - Vector|Tensor field rendering
        // /////////////////////////////////////////////////////////////////////////////

        Slide   {
            title: "Vector|Tensor field rendering"

            section: "4 - Vector|Tensor field rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "4.1 - Overview",
                "4.2 - Vectors",
                "4.3 - Tensors",
            ]
        }

        Slide   {
            title: "Overview"

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.1 - Overview";
            subsubsection: "";

            content: [
                "A vector is  a quantity with magnitude and direction (e.g. velocity of a particle)",
                "A vector field simply is a distribution of vectors over space",
                "A tensor is generalisation of a vector, represented by an array of components that are functions of the coordinates of a space (e.g. velocity TW/ statistics)",
                "A tensor field simply is a distribution of tensors over space",
            ]
        }

        Slide   {
            title: "Vectors"

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Vectors";
            subsubsection: "";

            content: [
                "4.2.1 - Glyphs",
                "4.2.2 - Streamlines",
                "4.2.3 - Streamsurfaces",
            ]
        }

        Slide   {
            title: "Glyphs"

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Vectors";
            subsubsection: "4.2.1 - Glyphs";

            badges: [
                "vtkGlyphs3D",
                "vtkArrowSource",
                "vtkPolyData",
                "vtkPolyDataMapper",
            ]

            content: [
                "Natural visualisation method that draws oriented, scaled avatars for each vector",
                "Avatar could be anything:",
                " Line",
                " Arrow (2D)",
                " Arrow (3D)",
                " Any mesh",
            ]
        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "4 - Vector|Tensor field rendering";
            subsection: "VTK";
            subsubsection: "Code samples";

            source: Helper.read("/res/examples/cpp/glyph.cpp");
        }

        Slide   {
            title: "Streamlines";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Vectors";
            subsubsection: "4.2.2 - Streamlines";

            badges: [
                "vtkStreamTracer",
                "vtkPolyData",
                "vtkPolyDataMapper",
            ]

            content: [
                "Connect every vertex position over many time steps in 2D",
                "Results in numerical approximation to a particle trace",
                "Can be extruded WRT/ magnitude of vectors",
                "Can be colored WRT/ velocity magnitude of vectors",
            ]
        }

        Slide   {
            title: "Streamlines";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Vectors";
            subsubsection: "4.2.2 - Streamlines";

            badges: [
                "vtkStreamTracer",
                "vtkPolyData",
                "vtkPolyDataMapper",
            ]

            content: [
                "Numerical integration of an ODE",
                " Euler method",
                " Runge Kutta",
                "Discretisation",
            ]
        }

        CodeSlide {
            title: "<code>C++</code> counterpart";

            section: "4 - Vector|Tensor field rendering";
            subsection: "VTK";
            subsubsection: "Code samples";

            source: Helper.read("/res/examples/cpp/streamline.cpp");
        }

        Slide   {
            title: "Streamsurfaces";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Vectors";
            subsubsection: "4.2.3 - Streamsurfaces";

            badges: [
                "vtkStreamTracer",
                "vtkPolyData",
                "vtkPolyDataMapper",
            ]

            content: [
                "Connect every vertex position over many time steps in 3D",
                "Results in numerical approximation to a particle trace",
                "Can be extruded WRT/ magnitude of vectors",
                "Can be colored WRT/ velocity magnitude of vectors",
            ]
        }

        Slide   {
            title: "Tensors";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Tensors";
            subsubsection: "";

            badges: [
                "vtkTensorGlyph",
                "vtkVertexGlyphFilter",
            ]

            content: [
                "4.2.1 - Motivation",
                "4.2.2 - Diagonalisation",
                "4.2.3 - Interpretation",
                "4.2.4 - Rendering",
            ]
        }

        Slide   {
            title: "Motivation";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Tensors";
            subsubsection: "4.2.1 - Motivation";

            badges: [
                "vtkTensorGlyph",
                "vtkVertexGlyphFilter",
            ]

            content: [
                "Derivation of vector fields",
                "Metrics (curvature, length, area)",
                "Stress force in mechanics",
                "...",
            ]
        }

        Slide   {
            title: "Diagonalisation";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Tensors";
            subsubsection: "4.2.2 - Diagonalisation";

            badges: [
                "vtkTensorGlyph",
                "vtkVertexGlyphFilter",
            ]

            content: [
                "If for each vertex <i>v</i>",
                " If <i>f(v)</i> is a symmetric matrix",
                "  <i>f(v)ij - f(v)ji</i> for each <i>(i,j)</i>",
                "It can be diagonalised",
                " Using eigenvalues",
                " Using eigenvectors",
            ]
        }

        Slide   {
            title: "Interpretation";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Tensors";
            subsubsection: "4.2.3 - Interpretation";

            badges: [
                "vtkTensorGlyph",
                "vtkVertexGlyphFilter",
            ]

            content: [
                "Let's represent <i>f(v)</i> by an ellipsoid",
                " Semi principal axes are defined by eigenvectors",
                " Axis length is defined by eigenvalues",
                "Similarly to:",
                " Vector field angle",
                " Vector field magnitude",
            ]
        }

        Slide   {
            title: "Rendering";

            section: "4 - Vector|Tensor field rendering";
            subsection: "4.2 - Tensors";
            subsubsection: "4.2.4 - Rendering";

            badges: [
                "vtkTensorGlyph",
                "vtkVertexGlyphFilter",
            ]

            content: [
                "Direct representation:",
                " Glyphs packing",
                "Derived scalar fields or direction fields:",
                " Hyper-streamlines",
                " Hyper-LIC",
            ]
        }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Load the data set <code>data/vectorFields/2-grid/output.vti</code>",
        //         "A time step of time-varying CFD simulation",
        //         " Fluid is continuously injected from left to right",
        //         " Obstacle on the left",
        //         " Creates vortices (regular pattern <i>aka</i> Karman vortex street)",
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-1.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "What scalar quantity (derived from the vector fields) could be useful to analyse vortices?",
        //         "Find and apply the appropriate filter that computes several types of vector derivatives",
        //         "The quantity we are interested in is given by the <i>z</i> component of the 'vorticity' cell based vector field",
        //         "Use a color map to render and distinguish:",
        //         " clockwise vortices",
        //         " counter clockwise vortices"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-2.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Use the <code>Calculator</code> filter to extract an actual scalar field from the <i>z</i> component of the 'vorticity' vector field",
        //         "Based on the thresholds of the previous color mapping, use the <code>Threshold</code> filter to extract the vortices",
        //         "Display them on top of the original magnitude scalar field"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-3.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Glyphs allow to draw actual vectors on specific points of the domain",
        //         "Apply the <code>Glyph</code> filter"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-4.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the data set <code>data/vectorFields/3-triangulation/output.vtu</code>",
                "Display transluscent boundaries",
                "Apply the <code>Glyph</code> filter",
                "It looks like the vector field uniformly points downwards ..."
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-5.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Hide the glyphs",
                "Find and apply the appropriate filters to:",
                " Extract streamlines",
                " Enhance the rendering with tubular surfaces"
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-6.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Use volume rendering to display the domain"
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-7.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Display the glyphs",
                "Find and apply the relevant filters to:",
                " Provide visual insight about orientation",
                " Provide visual insight about magnitude",
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-8.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-9.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the data set <code>data/vectorFields/3-grid/output.vti</code>",
                "Try and reproduce the following visualisation"
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-10.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Load the data set <code>data/scalarFields/3-triangulation/output.vtu",
        //         "Extract an isovalue 63.5",
        //         "Notice the tunnel",
        //         "As the value increases, the tunnel will collapse"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-11.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Find and apply the relevant filter to generate the gradient vector field for this data set",
        //         "Seed streamlines exactly where the tunnel collapses",
        //         "Make sure the neighborhood around the seed point is small enough",
        //         "Color forward and backward streamlines distinctively",
        //         "How do we interpret this phenomenon?"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-12.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Extract isosurfaces on the original data set",
        //         "Select relevant components using <code>Extract cells by region</code>",
        //         "How do we interpret the rendering?"
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-13.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Try and identify the most prominents critical points",
        //         "Mark them with a sphere",
        //         "For each saddle, extract its separatices",
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-14.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        // Slide   {
        //     title: "Hands on session"

        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     content: [
        //         "Compute forward and backward streamlines:",
        //         " At various seeds",
        //         " One different color each",
        //         "What does the rendering highlights?",
        //     ]
        // }

        // Slide   {
        //     section: "4 - Vector|Tensor field rendering";
        //     subsection: "Hands on session";
        //     subsubsection: "Paraview";

        //     Image {
        //         source: "4-15.jpg";

        //         x: -parent.x;
        //         y: -parent.y;

        //         width: presentation.width;
        //         height: presentation.height - 32;

        //         fillMode: Image.PreserveAspectFit;
        //     }
        // }

        Slide   {
            title: "Hands on session"

            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            content: [
                "Load the data set <code>data/tensorFields/3-triangulation/output.vtu</code>",
                "Apply various clippings",
                "Find and apply the appropriate filters to display glyphs representing:",
                " The eigenvalues of the tensor field",
                " The directions of the tensor field"
            ]
        }

        Slide   {
            section: "4 - Vector|Tensor field rendering";
            subsection: "Hands on session";
            subsubsection: "Paraview";

            Image {
                source: "4-16.jpg";

                x: -parent.x;
                y: -parent.y;

                width: presentation.width;
                height: presentation.height - 32;

                fillMode: Image.PreserveAspectFit;
            }
        }

        // /////////////////////////////////////////////////////////////////////////////
        // 5 - In situ & Parallel rendering
        // /////////////////////////////////////////////////////////////////////////////

        Slide   {
            title: "In situ & Parallel rendering";

            section: "5 - In situ & Parallel rendering";
            subsection: "";
            subsubsection: "";

            content: [
                "1 - In situ rendering",
                "2 - Parallel rendering",
            ]
        }

        Slide   {
            title: "In situ rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.1 - In situ rendering";
            subsubsection: "";

            content: [
                "A solution to ever growing sophistication of data and rendering algorithms",
                " CPU/GPU consuming rendering",
                " Memory consuming data",
                "In situ == co-processing",
                " Slaves (as in MPI) handle the work",
            ]
        }

        Slide   {
            title: "In situ rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.1 - In situ rendering";
            subsubsection: ""

            content: [
                "Extend with in situ pipeline rendering algorithms",
            ]

            Figure {
                figure: "insitu.png";
                margin: 20;
                height: parent.height * 0.8;
            }
        }

        Slide   {
            title: "In situ rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.1 - In situ rendering";
            subsubsection: ""

            content: [
                "Extend with in situ pipeline rendering algorithms",
                " Using adaptors",
            ]

            Figure {
                figure: "024";
                margin: 20;

                height: parent.height * 0.8;
            }
        }

        Slide   {
            title: "In situ rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.1 - In situ rendering";
            subsubsection: ""

            content: [
                "Add <code>Python</code> scripts to locally extend the pipeline",
                "Pipeline specification to execute on each time step",
                "Produces:",
                " Output images: for rendering",
                " Output data: if needed",
            ]
        }

        CodeSlide {
            title: "In situ rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.1 - In situ rendering";
            subsubsection: ""

            source: Helper.read("/res/examples/cpp/catalyst.cpp");
        }

        Slide   {
            title: "Parallel rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "";

            content: [
                "Visualise large models:",
                " Don't fit in memory",
                " Postprocessing is not a solution",
                "Using Paraview",
            ]
        }

        Slide   {
            title: "Parallel rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "";

            content: [
                "5.2.1 - Architecture",
                "5.2.2 - Partitioning",
                "5.2.3 - Ghosting",
                "5.2.4 - Rendering",
            ]
        }

        Slide   {
            title: "Architecture";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.1 - Architecture";

            content: [
                "Three tier architecture:",
                " Data server: unit responsible for data reading, filtering and writing",
                " Render server: unit responsible for rendering",
                " Client: unit responsible for establishing visualisation",
                "  controls object creation and destruction on the server",
                "  controls pipeline execution on the server",
            ]
        }

        Slide   {
            title: "Modes";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.1 - Architecture";

            content: [
                "Three run modes:",
                " Standalone: All units combined into a serial application",
                " Client-Server: <code>pvserver</code> on a parallel machine + <code>paraview</code> client",
                " Client-Render Server-Data mode: more sockets, almost never used",
            ]

            Figure {
                figure: "RunModeStandalone";
            }
        }

        Slide   {
            title: "Modes";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.1 - Architecture";

            content: [
                "Three run modes:",
                " Standalone: All units combined into a serial application",
                " Client-Server: <code>pvserver</code> on a parallel machine + <code>paraview</code> client",
                " Client-Render Server-Data mode: more sockets, almost never used",
            ]

            Figure {
                figure: "RunModeClientServer";
            }
        }

        Slide   {
            title: "Modes";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.1 - Architecture";

            content: [
                "Three run modes:",
                " Standalone: All units combined into a serial application",
                " Client-Server: <code>pvserver</code> on a parallel machine + <code>paraview</code> client",
                " Client-Render Server-Data mode: more sockets, almost never used",
            ]

            Figure {
                figure: "RunModeClientRenderDataServer";
            }
        }

        Slide   {
            title: "Partitioning";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.2 - Partitioning";

            content: [
                "Data is already broken into little pieces by the cells",
                "Create as many pieces as many processors we deal with",
            ]

            Figure {
                figure: "ParallelExampleMesh";
            }
        }

        Slide   {
            title: "Partitioning";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.2 - Partitioning";

            content: [
                "We assume 3 processors",
            ]

            Figure {
                figure: "ParallelExamplePartitions";
            }
        }

        Slide   {
            title: "Partitioning";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.2 - Partitioning";

            content: [
                "Some algorithms will work by allowing each process to independently run on its local collection of cells",
                "Example for a clipping algorithm",
            ]

            Figure {
                figure: "ParallelExampleClip1";
            }
        }

        Slide   {
            title: "Partitioning";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.2 - Partitioning";

            content: [
                "Data gathering is never done",
                "But visually ...",
            ]

            Figure {
                figure: "ParallelExampleClip2";
            }
        }

        Slide   {
            title: "Ghosting";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.3 - Ghosting";

            content: [
                "Blindly running algorithms on cell partitions is not always correct",
                "Consider finding all the external faces (ones that contribute to the boundary) of a mesh",
                "Why does this fail?",
            ]

            Figure {
                figure: "ParallelExampleExternalFaces1";
            }
        }

        Slide   {
            title: "Ghosting";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.3 - Ghosting";

            content: [
                "We need neighborhood information",
                "Ghost cells are cells that are held in one process but actually belongs to another",
                "The result is still locally wrong",
                "But globally right",
                "Paraview strips off the ghost cells",
            ]
        }

        Slide   {
            title: "Ghosting";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.3 - Ghosting";

            content: [
                "We need neighborhood information",
                "Ghost cells are cells that are held in one process but actually belongs to another",
                "The result is still locally wrong",
                "But globally right",
                "Paraview strips off the ghost cells",
            ]

            Figure {
                figure: "ParallelExampleExternalFaces2";

                height: parent.height * 0.6;
            }
        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            content: [
                "Data is partitioned among all processes up to an including rendering process",
                "<code>IceT</code> is the library responsible for parallel rendering",
                " Sort-last rendering algorithm",
                "  Local render on each partition",
                "  Compositing",
            ]

        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            Figure {
                figure: "ParallelRendering";

                height: parent.height;
            }
        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            content: [
                "Multiple parallel image composition algorithms:",
                " Binary tree",
                " Binary swap",
            ]

        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            Figure {
                figure: "ParallelRenderingDetail";

                height: parent.height;
            }
        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            content: [
                "Sort-last algorithm is actually insensitive to the amount of data being dealt with",
                " Efficient",
                " Scalable",
                "Bottleneck: the resolution of the rendered image",
            ]
        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            content: [
                "Handling of tiled displays",
                "Increases the number of pixels to render",
                "Use spatiality! (D3 filter)",
            ]
        }

        Slide   {
            title: "Rendering";

            section: "5 - In situ & Parallel rendering ";
            subsection: "5.2 - Parallel rendering";
            subsubsection: "5.2.4 - Rendering";

            Figure {
                figure: "ParallelRenderingTiles";

                height: parent.height;
            }
        }

        // /////////////////////////////////////////////////////////////////////////////
        // A - Coding sessions
        // /////////////////////////////////////////////////////////////////////////////

        Slide   {
            title: "Coding sessions";

            section: "A - Coding sessions";
            subsection: "";
            subsubsection: "";

            content: [
                "A.1 - Environment setup",
                "A.2 - Python examples",
                "A.2 - C++ examples",
            ]
        }

        Slide   {
            title: "Coding sessions";

            section: "A - Coding sessions";
            subsection: "";
            subsubsection: "";

            content: [
                "Useful links",
                " Conda - <code>https://anaconda.org</code>",
                " VTK Examples",
                "  Python - <code>https://kitware.github.io/vtk-examples/site/Python/</code>",
                "  C++    - <code>https://kitware.github.io/vtk-examples/site/Cxx/</code>",
            ]
        }

        Slide   {
            title: "Environment setup";

            section: "A - Coding sessions";
            subsection: "A.1 - Environment setup";
            subsubsection: "";

            content: [
                "WSL - Windows subsystem 4 linux",
                "<code>https://docs.conda.io/en/latest/miniconda.html</code>",
                " Choose linux-64",
                " <code>chmod u+x ./Miniconda3-latest-Linux-x86_64.sh</code>",
                " <code>./Miniconda3-latest-Linux-x86_64.sh</code>",
                " Install in <code>$HOME/.conda</code>",
                " Accept license terms",
                " Accept shell configuration",
                " Reload shell",
                " Test with: <code>which conda</code>",
            ]
        }

        Slide   {
            title: "Environment setup";

            section: "A - Coding sessions";
            subsection: "A.1 - Environment setup";
            subsubsection: "";

            content: [
                "Create environment:",
                " <code>conda create -n lecture-visu</code>",
                "Install dependencies:",
                " <code>conda activate lecture-visu</code>",
                " <code>conda install -c dtk-forge-staging -c conda-forge vtk=8.2.0</code>",
            ]
        }

        Slide   {
            title: "Python examples";

            section: "A - Coding sessions";
            subsection: "A.1 - Python examples";
            subsubsection: "";

            content: [
                "Run the code sample:",
                " <code>conda activate lecture-visu</code>",
                " <code>cd samples/py</code>",
                " <code>python ./A11.py</code>",
            ]
        }

        Slide   {
            title: "Python examples";

            section: "A - Coding sessions";
            subsection: "A.1 - Python examples";
            subsubsection: "";

            content: [
                "Modify it so that:",
                " It generates a 2D 256x100 2D image",
                " Representing a monochrome linear gradient along the <code>x</code> direction",
                " Get rid of the visualisation",
                " Keep the code clean and minimal",
            ]

            Figure {
                figure: "A11-t";
            }
        }

        CodeSlide {
            title: "Python example";

            section: "A - Coding sessions";
            subsection: "A.1 - Python examples";
            subsubsection: ""

            source: Helper.read("/samples/py/A11-t.py");
        }

        Slide   {
            title: "C++ examples";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: "";

            content: [
                "Run the code sample:",
                " <code>conda activate lecture-visu</code>",
                " <code>cd samples/cpp</code>",
                " <code>mkdir build</code>",
                " <code>cd build</code>",
                " <code>cmake ..</code>",
                " <code>make</code>",
                " <code>./A21</code>",
            ]
        }

        Slide   {
            title: "C++ examples";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: "";

            content: [
                "Make it sexy, modify it so that:",
                " It uses a dark background",
                " It uses a better interaction style (trackball camera)",
                " The sphere is -also- rendered:",
                "  As a smooth transluscent surface",
                "  As a wireframe opaque dataset",
                " The octree is rendered:",
                "  Using tubes (no tube filter)",
            ]
        }

        Slide   {
            title: "C++ examples";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: "";

            content: [
                "Make it ~somehow~ useful, modify it so that:",
                " Use the octree to find closest points to one the sphere's pole",
                " With half the radius of the sphere's one",
                " Display them as glyphs",
            ]

            Figure {
                figure: "A21-t";

                height: parent.height * 0.55;
            }
        }

        CodeSlide {
            title: "C++ example";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: ""

            source: Helper.read("/samples/cpp/A21-s.cpp");
        }

        Slide   {
            title: "C++ examples";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: "";

            content: [
                "Modify it so that:",
                " The slider's value moves the extraction point",
                " e.g. <code>double pole[3] = { 0.5 - this->Level * 1.0 / 6.0 , 0.0, 0.0 };</code>",
                " The extraction visualisation gets updated",
            ]

            Figure {
                figure: "A21-t-2";

                height: parent.height * 0.55;
            }
        }

        CodeSlide {
            title: "C++ example";

            section: "A - Coding sessions";
            subsection: "A.2 - C++ examples";
            subsubsection: ""

            source: Helper.read("/samples/cpp/A21-t.cpp");
        }

        onCurrentSlideChanged: {

            if (presentation.currentSlide === 6) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/PolyData.qml");
            } else if (presentation.currentSlide === 7) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/pure/Clock.qml");
            } else if (presentation.currentSlide === 20) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/simple/Property.qml");
            } else if (presentation.currentSlide === 29) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/simple/Prop3D.qml");
            } else if (presentation.currentSlide === 55) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/Quadric.qml");
            } else if (presentation.currentSlide === 56) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/ElevatedSphere.qml");
            } else if (presentation.currentSlide === 57) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/TransformedElevatedSphere.qml");
            } else if (presentation.currentSlide === 58) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/Mace.qml");
            } else if (presentation.currentSlide === 59) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/LoopShrink.qml");
            } else if (presentation.currentSlide === 79) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/simple/CylinderSource.qml");
            } else if (presentation.currentSlide === 81) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/simple/DICOMImageReader.qml");
            } else if (presentation.currentSlide === 85) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/lecture/PolyData.qml");
            } else if (presentation.currentSlide === 127) {
                showcase.visible = true;
                showcase.closeFile();
                showcase.openFile("/res/examples/qml/simple/GPUVolumeRayCastMapper.qml");
            } else {
                showcase.visible = false;
            }
        }
    }
}

//
// deck.qml ends here
