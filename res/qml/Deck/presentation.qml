// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

import QtQuick 2.5
import QtQuick.Window 2.0

import Deck 1.0 as Deck
import UI 1.0 as UI

Item {
    id: root

    property variant slides: []
    property int currentSlide: 0

    property bool showNotes: false;
    property bool allowDelay: true;
    property alias mouseNavigation: mouseArea.enabled
    property bool arrowNavigation: true
    property bool keyShortcutsEnabled: true

    property color titleColor: textColor;
    property color textColor: "black"
    property color footerColor: "black"

    property string fontFamily: "Helvetica"
    property string codeFontFamily: "Courier New"

    // Enhanced API
    property string section: "";
    property string subsection: "";

    // Private API
    property bool _faded: false
    property int _userNum;
    property int _lastShownSlide: 0

    Component.onCompleted: {
        var slideCount = 0;
        var slides = [];
        for (var i=0; i<root.children.length; ++i) {
            var r = root.children[i];
            if (r.isSlide) {
                slides.push(r);
            }
        }

        root.slides = slides;
        root._userNum = 0;

        // Make first slide visible...
        if (root.slides.length > 0)
            root.slides[root.currentSlide].visible = true;
    }

    function switchSlides(from, to, forward) {
        from.visible = false
        to.visible = true
        return true
    }

    onCurrentSlideChanged: {
        switchSlides(root.slides[_lastShownSlide], root.slides[currentSlide], currentSlide > _lastShownSlide)
        _lastShownSlide = currentSlide
    }

    function goToNextSlide() {
        root._userNum = 0
        if (_faded)
            return
        if (root.slides[currentSlide].delayPoints) {
            if (root.slides[currentSlide]._advance())
                return;
        }
        if (currentSlide + 1 < root.slides.length)
            ++currentSlide;
    }

    function goToPreviousSlide() {
        root._userNum = 0
        if (root._faded)
            return
        if (currentSlide - 1 >= 0)
            --currentSlide;
    }

    function goToUserSlide() {
        --_userNum;
        if (root._faded || _userNum >= root.slides.length)
            return
        if (_userNum < 0)
            goToNextSlide()
        else {
            currentSlide = _userNum;
            root.focus = true;
        }
    }

    // directly type in the slide number: depends on root having focus
    Keys.onPressed: {
        if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9)
            _userNum = 10 * _userNum + (event.key - Qt.Key_0)
        else {
            if (event.key == Qt.Key_Return || event.key == Qt.Key_Enter)
                goToUserSlide();
            _userNum = 0;
        }
    }

    // navigate with arrow keys
    Shortcut { sequence: StandardKey.MoveToNextLine; enabled: root.arrowNavigation; onActivated: goToNextSlide() }
    Shortcut { sequence: StandardKey.MoveToPreviousLine; enabled: root.arrowNavigation; onActivated: goToPreviousSlide() }
    Shortcut { sequence: StandardKey.MoveToNextChar; enabled: root.arrowNavigation; onActivated: goToNextSlide() }
    Shortcut { sequence: StandardKey.MoveToPreviousChar; enabled: root.arrowNavigation; onActivated: goToPreviousSlide() }

    // presentation-specific single-key shortcuts (which interfere with normal typing)
    Shortcut { sequence: " "; enabled: root.keyShortcutsEnabled; onActivated: goToNextSlide() }
    Shortcut { sequence: "c"; enabled: root.keyShortcutsEnabled; onActivated: root._faded = !root._faded }

    // standard shortcuts
    Shortcut { sequence: StandardKey.MoveToNextPage; onActivated: goToNextSlide() }
    Shortcut { sequence: StandardKey.MoveToPreviousPage; onActivated: goToPreviousSlide() }
    Shortcut { sequence: StandardKey.Quit; onActivated: Qt.quit() }

    Rectangle {
        z: 1000
        color: "black"
        anchors.fill: parent
        opacity: root._faded ? 1 : 0
        Behavior on opacity { NumberAnimation { duration: 250 } }
    }

    Rectangle {
        id: footer_l;

        color: footerColor;

        anchors.bottom: parent.bottom;
        anchors.left: parent.left;

        height: 34;
        width: parent.width / 3;

        visible: slides[currentSlide].section.length;

        UI.Icon {
            anchors.left: parent.left;
            anchors.leftMargin: 16;
            anchors.verticalCenter: parent.verticalCenter;

            icon: icons.fa_book;
            color: "#9DA5B4";
        }

        Text {
            text: slides[currentSlide].section;

            color: "#9DA5B4";

            anchors.left: parent.left;
            anchors.leftMargin: 32 + 4;
            anchors.verticalCenter: parent.verticalCenter;
        }
    }

    Rectangle {
        id: footer_m;

        color: footerColor;

        anchors.bottom: parent.bottom;
        anchors.left: footer_l.right;

        height: 34;
        width: parent.width / 3;

        visible: slides[currentSlide].section.length;

        UI.Icon {

            id: footer_m_icon;

            anchors.left: parent.left;
            anchors.leftMargin: 16;
            anchors.verticalCenter: parent.verticalCenter;

            icon: icons.fa_bookmark;
            color: "#9DA5B4";

            visible: slides[currentSlide].subsection.length > 1;
        }

        Text {
            text: slides[currentSlide].subsection;

            color: "#9DA5B4";

            anchors.left: parent.left;
            anchors.leftMargin: 32;
            anchors.verticalCenter: parent.verticalCenter;
        }
    }

    Rectangle {
        id: footer_r;

        color: footerColor;

        anchors.bottom: parent.bottom;
        anchors.right: parent.right;

        height: 34;
        width: parent.width / 3;

        visible: slides[currentSlide].section.length;

        UI.Icon {

            id: footer_r_icon;

            anchors.left: parent.left;
            anchors.leftMargin: 16;
            anchors.verticalCenter: parent.verticalCenter;

            icon: icons.fa_bookmark;
            color: "#9DA5B4";

            visible: slides[currentSlide].subsubsection.length > 1;
        }

        Text {
            text: slides[currentSlide].subsubsection;

            color: "#9DA5B4";

            anchors.left: parent.left;
            anchors.leftMargin: 32;
            anchors.verticalCenter: parent.verticalCenter;
        }

        Deck.SlideCounter {}
        Deck.Clock {}
    }

    Rectangle {
        id: footer_border_bottom;

        color: "#181A1F";

        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 34;
        anchors.left: parent.left;
        anchors.right: parent.right;

        height: 1;

        visible: slides[currentSlide].section.length;
    }

    Rectangle {
        id: footer_separator_1;

        color: "#181A1F";

        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        anchors.leftMargin: parent.width/3;

        width: 1;
        height: 34;

        visible: slides[currentSlide].section.length;
    }

    Rectangle {
        id: footer_separator_2;

        color: "#181A1F";

        anchors.bottom: parent.bottom;
        anchors.left: parent.left;
        anchors.leftMargin: 2*parent.width/3;

        width: 1;
        height: 34;

        visible: slides[currentSlide].section.length;
    }

    Column {
        id: badgesId;
        anchors.right: parent.right;
        anchors.top: parent.top;
        anchors.margins: 20;

        width: parent.width * 0.15;

        Repeater {
            model: slides[currentSlide].badges.length

            Row {
                id: row

                height: 30;

                UI.Badge {
                    label.text: slides[currentSlide].badges[index];
                    label.width: badgesId.width;
                    height: 26;
                    width: badgesId.width;
                    color: "#872ACB";
                    radius: 4;
                    border.color: "#dddddd";
                    label.horizontalAlignment: Text.AlignLeft;
                    visibility: true;
                }
            }
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button == Qt.RightButton)
                goToPreviousSlide()
            else
                goToNextSlide()
        }
        onPressAndHold: goToPreviousSlide(); //A back mechanism for touch only devices
    }

    Window {
        id: notesWindow;
        width: 400
        height: 300

        title: "QML Presentation: Notes"
        visible: root.showNotes

        Flickable {
            anchors.fill: parent
            contentWidth: parent.width
            contentHeight: textContainer.height

            Item {
                id: textContainer
                width: parent.width
                height: notesText.height + 2 * notesText.padding

                Text {
                    id: notesText

                    property real padding: 16;

                    x: padding
                    y: padding
                    width: parent.width - 2 * padding


                    font.pixelSize: 16
                    wrapMode: Text.WordWrap

                    property string notes: root.slides[root.currentSlide].notes;

                    onNotesChanged: {
                        var result = "";

                        var lines = notes.split("\n");
                        var beginNewLine = false
                        for (var i=0; i<lines.length; ++i) {
                            var line = lines[i].trim();
                            if (line.length == 0) {
                                beginNewLine = true;
                            } else {
                                if (beginNewLine && result.length) {
                                    result += "\n\n"
                                    beginNewLine = false
                                }
                                if (result.length > 0)
                                    result += " ";
                                result += line;
                            }
                        }

                        if (result.length == 0) {
                            font.italic = true;
                            text = "no notes.."
                        } else {
                            font.italic = false;
                            text = result;
                        }
                    }
                }
            }
        }
    }
}

// 
// presentation.qml ends here
