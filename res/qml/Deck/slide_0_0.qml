// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12
import QtQuick.Window 2.2

import Deck 1.0 as Deck

Deck.Slide {
    centeredText: "Scientific visualisation";
}
