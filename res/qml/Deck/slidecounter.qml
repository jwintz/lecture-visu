// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.0

import UI 1.0 as UI

Rectangle {

    id: counter;

    property color textColor: root.textColor;
    property color footerColor: root.footerColor;

    property string fontFamily: root.fontFamily;

    color: footerColor;

    anchors.bottom: parent.bottom;
    anchors.right: parent.right;
    anchors.rightMargin: width * 2 + 12 * 2;

    height: 34;
    width: header_r_counter.width + counter_text.width;

    UI.Icon {

        id: header_r_counter;

        height: 34;

        icon: icons.fa_hashtag;
        color: "#9DA5B4";

        anchors.left: parent.left;
        anchors.bottom: parent.bottom;
    }

    Text {

        id: counter_text;

        text: "" + (root.currentSlide + 1) + " / " + root.slides.length;

        color: counter.textColor;

        font.family: counter.fontFamily;

        anchors.left: header_r_counter.right;
        anchors.leftMargin: 6;
        anchors.bottom: parent.bottom;
        anchors.bottomMargin: 9;
    }

    Rectangle {
        id: counter_separator;

        color: "#181A1F";

        anchors.left: header_r_counter.left;
        anchors.leftMargin: -12;
        anchors.bottom: parent.bottom;

        width: 1;
        height: 34;

        visible: slides[currentSlide].section.length;
    }
}

//
// slidecounter.qml ends here
