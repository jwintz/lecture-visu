import QtQuick 2.12

import UI 1.0 as UI

Rectangle {
  id: root;

  width: label.width;
  height: label.height;

  property alias label: label;
  property alias visibility: tag.visible;

  color: "#1B1D23";

  radius: 2;

  UI.Icon {
    id: tag;
    icon: icons.fa_tag;
    anchors.verticalCenter: parent.verticalCenter;
    anchors.leftMargin: 10;
    anchors.left: parent.left;
    visible: false;
  }

  Label {
    id: label;

    anchors.verticalCenter: parent.verticalCenter;
    anchors.left: tag.visible ? tag.right : parent.left;
    leftPadding: 8;
    rightPadding: 8;
    font.weight: Font.Medium;
    font.pointSize: 11;
    font.family: "Fira Code";
    padding: 1;
  }
}
