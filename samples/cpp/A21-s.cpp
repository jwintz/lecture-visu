// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    double point[3];
    double pole[3] = { 0.5, 0.0, 0.0 };

    vtkNew<vtkIdList> result;
    vtkNew<vtkPoints> extraction;

    octree->FindPointsWithinRadius(0.25, pole, result);

    std::cout << "Found " << result->GetNumberOfIds() << " points" << std::endl;

    for (vtkIdType i = 0; i < result->GetNumberOfIds(); i++)
    {
        sphereSource->GetOutput()->GetPoint(result->GetId(i), point);

        std::cout << "Closest point " << i << ": Point " << result->GetId(i) << ": ("
                  << point[0] << ", " << point[1] << ", " << point[2] << ")" << std::endl;

        extraction->InsertNextPoint(point[0], point[1], point[2]);
    }

    vtkNew<vtkPolyData> extractionData;
    extractionData->SetPoints(extraction);

    vtkNew<vtkCubeSource> extractionShape;
    extractionShape->SetXLength(0.01);
    extractionShape->SetYLength(0.01);
    extractionShape->SetZLength(0.01);

    vtkNew<vtkGlyph3D> extractionGlyph;
    extractionGlyph->SetSourceConnection(extractionShape->GetOutputPort());
    extractionGlyph->SetInputData(extractionData);
    extractionGlyph->ScalingOff();
    extractionGlyph->Update();

    vtkNew<vtkPolyDataMapper> extractionMapper;
    extractionMapper->SetInputConnection(extractionGlyph->GetOutputPort());

    vtkNew<vtkActor> extractionActor;
    extractionActor->SetMapper(extractionMapper);
    extractionActor->GetProperty()->SetColor(colors->GetColor4d("orange_red").GetData());

// /////////////////////////////////////////////////////////////////////////////
