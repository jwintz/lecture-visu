#include <vtkActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>
#include <vtkCubeSource.h>
#include <vtkGlyph3D.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkMath.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkOctreePointLocator.h>
#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkSliderWidget.h>
#include <vtkSphereSource.h>
#include <vtkTextProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkWidgetEventTranslator.h>

#include <cmath>

namespace {

class vtkSliderCallback : public vtkCommand
{
public:
    static vtkSliderCallback* New()
    {
        return new vtkSliderCallback;
    }

    vtkSliderCallback() : Octree(0), Level(0), PolyData(0), Renderer(0)
    {

    }

    virtual void Execute(vtkObject* caller, unsigned long, void*)
    {
        vtkSliderWidget* sliderWidget = reinterpret_cast<vtkSliderWidget*>(caller);
        this->Level = vtkMath::Round(
            static_cast<vtkSliderRepresentation*>(sliderWidget->GetRepresentation())
            ->GetValue());

        this->Octree->GenerateRepresentation(this->Level, this->PolyData);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

        double point[3];

        double pole[3] = { 0.5 - this->Level * 1.0 / 6.0 , 0.0, 0.0 };

        vtkNew<vtkIdList> result;

        Octree->FindPointsWithinRadius(0.25, pole, result);

        std::cout << "Found " << result->GetNumberOfIds() << " points" << std::endl;

        Points->Initialize();

        for (vtkIdType i = 0; i < result->GetNumberOfIds(); i++)
        {
            Source->GetOutput()->GetPoint(result->GetId(i), point);

            std::cout << "Closest point " << i << ": Point " << result->GetId(i) << ": ("
                      << point[0] << ", " << point[1] << ", " << point[2] << ")" << std::endl;

            Points->InsertNextPoint(point[0], point[1], point[2]);
        }

        Glyphs->Update();

// /////////////////////////////////////////////////////////////////////////////

        this->Renderer->Render();
    }

    vtkOctreePointLocator* Octree;
    int Level;
    vtkPolyData* PolyData;
    vtkPoints *Points;
    vtkSphereSource *Source;
    vtkGlyph3D *Glyphs;
    vtkRenderer* Renderer;
};

} // namespace

int main(int, char*[])
{
    vtkNew<vtkNamedColors> colors;

    // Create interest data

    vtkNew<vtkSphereSource> sphereSource;
    sphereSource->SetPhiResolution(50);
    sphereSource->SetThetaResolution(50);

    vtkNew<vtkPolyDataMapper> sphereMapper;
    sphereMapper->SetInputConnection(sphereSource->GetOutputPort());
    sphereSource->Update();

    vtkNew<vtkActor> pointsActor;
    pointsActor->SetMapper(sphereMapper);
    pointsActor->GetProperty()->SetInterpolationToFlat();
    pointsActor->GetProperty()->SetRepresentationToPoints();
    pointsActor->GetProperty()->RenderPointsAsSpheresOn();
    pointsActor->GetProperty()->SetPointSize(8);
    pointsActor->GetProperty()->SetColor(colors->GetColor4d("Yellow").GetData());

    vtkNew<vtkActor> sphereActor;
    sphereActor->SetMapper(sphereMapper);
    sphereActor->GetProperty()->SetInterpolationToGouraud();
    sphereActor->GetProperty()->SetOpacity(0.7);
    sphereActor->GetProperty()->SetColor(colors->GetColor4d("alice_blue").GetData());

    vtkNew<vtkActor> wireframeActor;
    wireframeActor->SetMapper(sphereMapper);
    wireframeActor->GetProperty()->SetRepresentationToWireframe();
    wireframeActor->GetProperty()->LightingOff();
    wireframeActor->GetProperty()->SetLineWidth(2.0);
    wireframeActor->GetProperty()->SetColor(colors->GetColor4d("mint_cream").GetData());

    // Create the tree

    vtkNew<vtkOctreePointLocator> octree;
    octree->SetMaximumPointsPerRegion(5);
    octree->SetDataSet(sphereSource->GetOutput());
    octree->BuildLocator();

    vtkNew<vtkPolyData> polydata;
    octree->GenerateRepresentation(0, polydata);

    vtkNew<vtkPolyDataMapper> octreeMapper;
    octreeMapper->SetInputData(polydata);

    vtkNew<vtkActor> octreeActor;
    octreeActor->SetMapper(octreeMapper);
    octreeActor->GetProperty()->SetRepresentationToWireframe();
    octreeActor->GetProperty()->SetRenderLinesAsTubes(1);
    octreeActor->GetProperty()->SetLineWidth(5.0);
    octreeActor->GetProperty()->SetColor(
        colors->GetColor4d("SpringGreen").GetData());

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    vtkNew<vtkPoints> extraction;

    vtkNew<vtkPolyData> extractionData;
    extractionData->SetPoints(extraction);

    vtkNew<vtkCubeSource> extractionShape;
    extractionShape->SetXLength(0.01);
    extractionShape->SetYLength(0.01);
    extractionShape->SetZLength(0.01);

    vtkNew<vtkGlyph3D> extractionGlyph;
    extractionGlyph->SetSourceConnection(extractionShape->GetOutputPort());
    extractionGlyph->SetInputData(extractionData);
    extractionGlyph->ScalingOff();
    extractionGlyph->Update();

    vtkNew<vtkPolyDataMapper> extractionMapper;
    extractionMapper->SetInputConnection(extractionGlyph->GetOutputPort());

    vtkNew<vtkActor> extractionActor;
    extractionActor->SetMapper(extractionMapper);
    extractionActor->GetProperty()->SetColor(colors->GetColor4d("orange_red").GetData());

// /////////////////////////////////////////////////////////////////////////////

    // A renderer and render window

    vtkNew<vtkRenderer> renderer;
    vtkNew<vtkRenderWindow> renderWindow;
    renderWindow->AddRenderer(renderer);

    // An interactor

    vtkNew<vtkInteractorStyleTrackballCamera> style;

    vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
    renderWindowInteractor->SetInteractorStyle(style);
    renderWindowInteractor->SetRenderWindow(renderWindow);

    // Add the actors to the scene

    renderer->AddActor(pointsActor);
    renderer->AddActor(sphereActor);
    renderer->AddActor(wireframeActor);
    renderer->AddActor(octreeActor);
    renderer->AddActor(extractionActor);
    renderer->SetBackground(colors->GetColor3d("ivory_black").GetData());

    // Render an image (lights and cameras are created automatically)

    renderWindow->SetWindowName("OctreeVisualize");
    renderWindow->SetSize(600, 600);
    renderWindow->Render();

    // An interactive widget

    vtkNew<vtkSliderRepresentation2D> sliderRep;
    sliderRep->SetMinimumValue(0);
    sliderRep->SetMaximumValue(octree->GetLevel());
    sliderRep->SetValue(0);
    sliderRep->SetTitleText("Level");
    sliderRep->GetPoint1Coordinate()->SetCoordinateSystemToNormalizedDisplay();
    sliderRep->GetPoint1Coordinate()->SetValue(.2, .2);
    sliderRep->GetPoint2Coordinate()->SetCoordinateSystemToNormalizedDisplay();
    sliderRep->GetPoint2Coordinate()->SetValue(.8, .2);
    sliderRep->SetSliderLength(0.075);
    sliderRep->SetSliderWidth(0.05);
    sliderRep->SetEndCapLength(0.05);
    sliderRep->GetTitleProperty()->SetColor(
        colors->GetColor3d("Beige").GetData());
    sliderRep->GetCapProperty()->SetColor(
        colors->GetColor3d("MistyRose").GetData());
    sliderRep->GetSliderProperty()->SetColor(
        colors->GetColor3d("LightBlue").GetData());
    sliderRep->GetSelectedProperty()->SetColor(
        colors->GetColor3d("Violet").GetData());

    vtkNew<vtkSliderWidget> sliderWidget;
    sliderWidget->SetInteractor(renderWindowInteractor);
    sliderWidget->SetRepresentation(sliderRep);
    sliderWidget->SetAnimationModeToAnimate();
    sliderWidget->EnabledOn();

    vtkNew<vtkSliderCallback> callback;
    callback->Source = sphereSource;
    callback->Octree = octree;
    callback->PolyData = polydata;
    callback->Points = extraction;
    callback->Glyphs = extractionGlyph;
    callback->Renderer = renderer;
    callback->Execute(sliderWidget, 0, 0);

    sliderWidget->AddObserver(vtkCommand::InteractionEvent, callback);

    // Launch the event loop

    renderWindowInteractor->Initialize();
    renderWindow->Render();

    renderWindowInteractor->Start();

    return EXIT_SUCCESS;
}
