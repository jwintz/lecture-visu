#!/usr/bin/env python3

from vtkmodules.vtkCommonCore import VTK_UNSIGNED_CHAR
from vtkmodules.vtkCommonDataModel import vtkImageData
from vtkmodules.vtkFiltersGeometry import vtkImageDataGeometryFilter
from vtkmodules.vtkIOImage import vtkPNGWriter

def get_program_parameters():
    import argparse
    description = 'Generate image data, edit data points, store and reload it.'
    epilogue = '''
   '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue)
    parser.add_argument('filename', help='A required vtk filename, e.g. A11-t.png.', nargs='?',
                        const='A11-t.png',
                        type=str, default='A11-t.png')
    args = parser.parse_args()
    return args.filename


def main():
    filename = get_program_parameters()

    imageData = vtkImageData()
    imageData.SetDimensions(256, 100, 1)
    imageData.AllocateScalars(VTK_UNSIGNED_CHAR, 1)

    dims = imageData.GetDimensions()

    # Build a monochrome gradient
    for y in range(dims[1]):
        for x in range(dims[0]):
            imageData.SetScalarComponentFromDouble(x, y, 0, 0, x)

    writer = vtkPNGWriter()
    writer.SetFileName(filename)
    writer.SetInputData(imageData)
    writer.Write()

if __name__ == '__main__':
    main()
