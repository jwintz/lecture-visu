// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkAlgorithm.hpp"
#include "quickVtkProp.hpp"

namespace quick {

namespace Vtk {

Qml::Register::AbstractClass<Algorithm> Algorithm::Register(true);

Algorithm::Algorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) : Vtk::Object(Object::Type::Algorithm), m_vtkObject(vtkObject)
{

}

auto Algorithm::setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) -> void
{
    this->m_vtkObject = vtkObject;
}

auto Algorithm::getVtkObject(void) -> vtkSmartPointer<vtkAlgorithm>
{
    return this->m_vtkObject;
}

auto Algorithm::setProp(Prop *prop) -> void
{
    this->m_prop = prop;

    for (auto alg : this->m_input) {
        alg->setProp(prop);
    }

    for (auto dat : this->m_inputData) {
        dat->setProp(prop);
    }
}

auto Algorithm::update(void) -> void
{
    if (this->m_input.empty() && this->m_inputData.empty()) {
        if (this->m_prop) {
            this->m_prop->update();
        }
    } else {
        for (Algorithm* alg : this->m_input) {
            alg->update();
        }

        for (DataObject* dat : this->m_inputData) {
            dat->update();
        }
   }
}

auto Algorithm::isValid(void) -> bool
{
    return true;
}

auto Algorithm::getChildren(void) -> QQmlListProperty<Object>
{
    return QQmlListProperty<Object>(this, 0, &appendChild, &childrenCount, &childAt, &clearChildren);
}

auto Algorithm::getInput(void) -> QQmlListProperty<Algorithm>
{
    return QQmlListProperty<Algorithm>(this, 0, &appendInput, &inputCount, &inputAt, &clearInputs);
}

auto Algorithm::getInputData(void) -> QQmlListProperty<DataObject>
{
    return QQmlListProperty<DataObject>(this, 0, &appendInputData, &inputDataCount, &inputDataAt, &clearInputData);
}

auto Algorithm::appendChild(QQmlListProperty<Object>* list, Object* object) -> void
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if(!parent)
        return;

    if(Algorithm *algorithm = qobject_cast<Algorithm *>(object)) {

        parent->m_input.append(algorithm);
        parent->m_children.append(algorithm);

        int count = parent->m_input.count() - 1;

        parent->getVtkObject()->SetInputConnection(count, algorithm->getVtkObject()->GetOutputPort());

        emit parent->inputChanged();

        parent->setProp(algorithm->m_prop);
    }

    if(DataObject *data = qobject_cast<DataObject *>(object)) {

        parent->m_inputData.append(data);
        parent->m_children.append(data);

        int count = parent->m_inputData.count() - 1;

        parent->getVtkObject()->SetInputDataObject(count, data->getVtkObject());

        emit parent->inputDataChanged();

        parent->setProp(data->getProp());
    }
}

auto Algorithm::appendInput(QQmlListProperty<Algorithm>* list, Algorithm* algorithm) -> void
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent && algorithm) {

        parent->m_input.append(algorithm);
        parent->m_children.append(algorithm);
       
        int count = parent->m_input.count() - 1;

        parent->getVtkObject()->SetInputConnection(count, algorithm->getVtkObject()->GetOutputPort());

        emit parent->inputChanged();

        parent->setProp(algorithm->m_prop);
    }
}

auto Algorithm::appendInputData(QQmlListProperty<DataObject> *list, DataObject *data) -> void
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent && data) {

        parent->m_inputData.append(data);
        parent->m_children.append(data);

        int count = parent->m_inputData.count() - 1;

        parent->getVtkObject()->SetInputDataObject(count, data->getVtkObject());

        emit parent->inputDataChanged();

        parent->setProp(data->getProp());
    }
}

auto Algorithm::childrenCount(QQmlListProperty<Object>* list) -> int
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_children.count();

    return 0;
}

auto Algorithm::inputCount(QQmlListProperty<Algorithm>* list) -> int
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_input.count();

    return 0;
}

auto Algorithm::inputDataCount(QQmlListProperty<DataObject> *list) -> int
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_inputData.count();
   
    return 0;
}

auto Algorithm::childAt(QQmlListProperty<Object>* list, int i) -> Object *
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_children.at(i);

    return 0;
}

auto Algorithm::inputAt(QQmlListProperty<Algorithm>* list, int i) -> Algorithm *
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_input.at(i);

    return 0;
}

auto Algorithm::inputDataAt(QQmlListProperty<DataObject>* list, int i) -> DataObject *
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent)
        return parent->m_inputData.at(i);

    return 0;
}

auto Algorithm::clearChildren(QQmlListProperty<Object>* list) -> void
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent) {
        parent->m_input.clear();
        parent->m_inputData.clear();
        parent->m_children.clear();
        emit parent->childrenChanged();
        parent->update();
    }
}

auto Algorithm::clearInputs(QQmlListProperty<Algorithm>* list) -> void
{
    auto parent = qobject_cast<Algorithm*>(list->object);

    if (parent) {
        parent->m_input.clear();
        emit parent->inputChanged();
        parent->update();
    }
}

auto Algorithm::clearInputData(QQmlListProperty<DataObject> *list) -> void
{
    auto parent = qobject_cast<Algorithm *>(list->object);

    if (parent) {
        parent->m_inputData.clear();
        emit parent->inputDataChanged();
        parent->update();
    }
}

Algorithm::~Algorithm(void)
{
    this->m_prop = nullptr;
}

} // namespace Vtk

} // namespace quick

//
// quickVtkAlgorithm.cpp ends here
