// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "quickQmlRegister.hpp"
#include "quickVtkObject.hpp"
#include "quickVtkDataSet.hpp"

#include <QQmlListProperty>
#include <QQmlParserStatus>

#include <vtkSmartPointer.h>
#include <vtkAlgorithm.h>

namespace quick {

namespace Vtk {

class Prop;
class DataObject;

class Algorithm : public Object, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QQmlListProperty<quick::Vtk::Object> children READ getChildren NOTIFY childrenChanged);
    Q_PROPERTY(QQmlListProperty<quick::Vtk::Algorithm> input READ getInput NOTIFY inputChanged);
    Q_PROPERTY(QQmlListProperty<quick::Vtk::DataObject> inputData READ getInputData NOTIFY inputDataChanged);
    Q_CLASSINFO("DefaultProperty", "children");

public:
    Algorithm(void) = delete;
    Algorithm(vtkSmartPointer<vtkAlgorithm>);
    auto setProp(Prop*) -> void;
    auto setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm>) -> void;
    auto getChildren(void) -> QQmlListProperty<Object>;
    auto getInput(void) -> QQmlListProperty<Algorithm>;
    auto getInputData(void) -> QQmlListProperty<DataObject>;
    auto getVtkObject(void) -> vtkSmartPointer<vtkAlgorithm>;
    virtual auto isValid(void) -> bool;
    static auto appendChild(QQmlListProperty<Object>*, Object*) -> void;
    static auto appendInput(QQmlListProperty<Algorithm>*, Algorithm*) -> void;
    static auto appendInputData(QQmlListProperty<DataObject>*, DataObject*) -> void;
    static auto childrenCount(QQmlListProperty<Object>*) -> int;
    static auto inputCount(QQmlListProperty<Algorithm>*) -> int;
    static auto inputDataCount(QQmlListProperty<DataObject>*) -> int;
    static auto childAt(QQmlListProperty<Object>*, int) -> Object*;
    static auto inputAt(QQmlListProperty<Algorithm>*, int) -> Algorithm*;
    static auto inputDataAt(QQmlListProperty<DataObject>*, int) -> DataObject*;
    static auto clearChildren(QQmlListProperty<Object>*) -> void;
    static auto clearInputs(QQmlListProperty<Algorithm>*) -> void;
    static auto clearInputData(QQmlListProperty<DataObject>*) -> void;
    ~Algorithm(void);

public slots:
    void update(void);

signals:
    void childrenChanged(void);
    void inputChanged(void);
    void inputDataChanged(void);

signals:
    void completed();

public:
    virtual void classBegin(void)
    {
        ; // emit completed();
    }

    virtual void componentComplete(void)
    {
        emit completed();
    }

private:
    static Qml::Register::AbstractClass<Algorithm> Register;

private:
    QList<Object*> m_children;
    QList<Algorithm*> m_input;
    QList<DataObject*> m_inputData;

private:
    vtkSmartPointer<vtkAlgorithm> m_vtkObject = nullptr;

private:
    Prop* m_prop = nullptr;
};

}

}

//
// quickVtkAlgorithm.hpp ends here
