// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkContourFilter.hpp"

namespace quick {

namespace Vtk {

class ContourFilterPrivate
{
public:
};

ContourFilter::ContourFilter(void) : PolyDataAlgorithm(vtkSmartPointer<vtkContourFilter>::New())
{
    d = new ContourFilterPrivate;

    this->m_vtkObject = vtkContourFilter::SafeDownCast(PolyDataAlgorithm::getVtkObject());
}

ContourFilter::ContourFilter(vtkSmartPointer<vtkContourFilter> vtkObject) : PolyDataAlgorithm(vtkObject)
{
    d = new ContourFilterPrivate;

    this->m_vtkObject = vtkContourFilter::SafeDownCast(PolyDataAlgorithm::getVtkObject());
}

auto ContourFilter::getVtkObject(void) -> vtkSmartPointer<vtkContourFilter>
{
    return this->m_vtkObject;
}

    ContourFilter::~ContourFilter(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<ContourFilter> ContourFilter::Register(true);

}

}

//
// quickVtkContourFilter.cpp ends here
