// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkPolyDataAlgorithm.hpp"

#include <vtkSmartPointer.h>
#include <vtkContourFilter.h>

namespace quick
{

namespace Vtk
{

class ContourFilter : public PolyDataAlgorithm
{
    Q_OBJECT

public:
             ContourFilter(void);
             ContourFilter(vtkSmartPointer<vtkContourFilter>);
    virtual ~ContourFilter(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkContourFilter>;

public slots:
    void generateValues(qreal v1, qreal v2, qreal v3)
    {
        this->m_vtkObject->GenerateValues(v1, v2, v3);
        this->update();
    }

private:
    class ContourFilterPrivate *d;

private:
    vtkSmartPointer<vtkContourFilter> m_vtkObject = nullptr;

private:
    static Qml::Register::Class<ContourFilter> Register;
};

} // vtk

} // quick

//
// quickVtkContourFilter.hpp ends here
