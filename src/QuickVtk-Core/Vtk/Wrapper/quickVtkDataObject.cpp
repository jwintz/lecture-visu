// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkDataObject.hpp"
#include "quickVtkProp.hpp"

namespace quick {

namespace Vtk {

DataObject::DataObject(vtkSmartPointer<vtkDataObject> vtkObject) : Object(Object::Type::Data), m_vtkObject(vtkObject)
{

}

DataObject::~DataObject(void)
{
    this->m_vtkObject = nullptr;
}

auto DataObject::getVtkObject(void) -> vtkSmartPointer<vtkDataObject>
{
    return this->m_vtkObject;
}

auto DataObject::setProp(Prop *prop) -> void
{
    this->m_prop = prop;
}

auto DataObject::getProp(void) -> Prop*
{
    return this->m_prop;
}

void DataObject::update(void)
{
    this->m_prop->update();
}

Qml::Register::AbstractClass<DataObject> DataObject::Register(true);

} // namespace Vtk

} // namespace quick

// 
// quickVtkDataObject.cpp ends here
