// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QQmlParserStatus>

#include "quickQmlRegister.hpp"
#include "quickVtkObject.hpp"

#include <vtkSmartPointer.h>
#include <vtkDataObject.h>

namespace quick {

namespace Vtk {

class Prop;

class DataObject : public Object, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)

public:
             DataObject(void) = delete;
             DataObject(vtkSmartPointer<vtkDataObject>);
    virtual ~DataObject(void);

    auto getVtkObject(void) -> vtkSmartPointer<vtkDataObject>;

    auto setProp(Prop*) -> void;
    auto getProp(void) -> Prop*;

signals:
    void completed();

public slots:
    void update(void);

public:
    virtual void classBegin(void)
    {
        ; // emit completed();
    }

    virtual void componentComplete(void)
    {
        emit completed();
    }

private:
    static Qml::Register::AbstractClass<DataObject> Register;

private:
    vtkSmartPointer<vtkDataObject> m_vtkObject = nullptr;

private:
    Prop* m_prop = nullptr;
};

} // namespace Vtk

} // namespace quick

//
// quickVtkDataObject.hpp ends here
