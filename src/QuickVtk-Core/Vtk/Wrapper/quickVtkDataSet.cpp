// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkDataSet.hpp"

namespace quick {

namespace Vtk {

DataSet::DataSet(vtkSmartPointer<vtkDataSet> vtkObject) : DataObject(vtkObject)
{
    this->m_vtkObject = vtkDataSet::SafeDownCast(vtkObject);
}

DataSet::~DataSet(void)
{
    this->m_vtkObject = nullptr;
}

auto DataSet::getVtkObject(void) -> vtkSmartPointer<vtkDataSet>
{
    return this->m_vtkObject;
}

Qml::Register::AbstractClass<DataSet> DataSet::Register(true);

} // namespace Vtk

} // namespace quick

//
// quickVtkDataSet.cpp ends here
