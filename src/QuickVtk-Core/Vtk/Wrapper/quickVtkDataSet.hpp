// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "quickQmlRegister.hpp"
#include "quickVtkDataObject.hpp"

#include <vtkSmartPointer.h>
#include <vtkDataSet.h>

namespace quick {

namespace Vtk {

class DataSet : public DataObject
{
    Q_OBJECT

public:
             DataSet(void) = delete;
             DataSet(vtkSmartPointer<vtkDataSet>);
    virtual ~DataSet(void);

    auto getVtkObject(void) -> vtkSmartPointer<vtkDataSet>;

private:
    static Qml::Register::AbstractClass<DataSet> Register;

private:
    vtkSmartPointer<vtkDataSet> m_vtkObject = nullptr;
};

} // namespace Vtk

} // namespace quick

//
// quickVtkDataSet.hpp ends here
