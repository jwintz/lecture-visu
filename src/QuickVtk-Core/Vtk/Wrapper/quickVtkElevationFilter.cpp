#include "quickVtkElevationFilter.hpp"

namespace quick {
    namespace Vtk {

        Qml::Register::Class<ElevationFilter> ElevationFilter::Register(true);

        ElevationFilter::ElevationFilter() : DataSetAlgorithm(vtkSmartPointer<vtkElevationFilter>::New()) {
            this->m_vtkObject = vtkElevationFilter::SafeDownCast(this->getVtkObject());

            this->m_lowPoint = new Math::Vector3([this](){
                this->m_vtkObject->SetLowPoint(this->m_lowPoint->getValues().data());
                this->update();
            });

            this->m_highPoint = new Math::Vector3([this](){
                this->m_vtkObject->SetHighPoint(this->m_highPoint->getValues().data());
                this->update();
            });
        }

        auto ElevationFilter::getLowPoint(void) -> Math::Vector3*
        {
            return m_lowPoint;
        }

        auto ElevationFilter::getHighPoint(void) -> Math::Vector3*
        {
            return m_highPoint;
        }
    }
}
