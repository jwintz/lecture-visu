// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "quickVtkDataSetAlgorithm.hpp"
#include "quickMathVector3.hpp"

#include <vtkElevationFilter.h>

namespace quick {
    namespace Vtk {

        class ElevationFilter : public DataSetAlgorithm {
            Q_OBJECT
            Q_PROPERTY(quick::Math::Vector3* lowPoint READ getLowPoint);
            Q_PROPERTY(quick::Math::Vector3* highPoint READ getHighPoint);
           
        private:
            static Qml::Register::Class<ElevationFilter> Register;
            vtkSmartPointer<vtkElevationFilter> m_vtkObject = nullptr;
            Math::Vector3* m_lowPoint = nullptr;
            Math::Vector3* m_highPoint = nullptr;
        public:
            ElevationFilter();
            auto getLowPoint(void) -> Math::Vector3*;
            auto getHighPoint(void) -> Math::Vector3*;
        };
    }
}

//
// quickVtkElevationFilter.hpp ends here
