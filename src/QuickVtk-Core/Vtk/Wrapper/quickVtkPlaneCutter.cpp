// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkPlaneCutter.hpp"

namespace quick {

namespace Vtk {

class PlaneCutterPrivate
{
public:
};

PlaneCutter::PlaneCutter(void) : DataSetAlgorithm(vtkSmartPointer<vtkPlaneCutter>::New())
{
    d = new PlaneCutterPrivate;

    this->m_vtkObject = vtkPlaneCutter::SafeDownCast(DataSetAlgorithm::getVtkObject());
    this->m_vtkObject->GeneratePolygonsOn();
    this->m_vtkObject->InterpolateAttributesOn();
}

PlaneCutter::PlaneCutter(vtkSmartPointer<vtkPlaneCutter> vtkObject) : DataSetAlgorithm(vtkObject)
{
    d = new PlaneCutterPrivate;

    this->m_vtkObject = vtkPlaneCutter::SafeDownCast(DataSetAlgorithm::getVtkObject());
    this->m_vtkObject->GeneratePolygonsOn();
    this->m_vtkObject->InterpolateAttributesOn();
}

auto PlaneCutter::getVtkObject(void) -> vtkSmartPointer<vtkPlaneCutter>
{
    return this->m_vtkObject;
}

PlaneCutter::~PlaneCutter(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<PlaneCutter> PlaneCutter::Register(true);

}

}

//
// quickVtkPlaneCutter.cpp ends here
