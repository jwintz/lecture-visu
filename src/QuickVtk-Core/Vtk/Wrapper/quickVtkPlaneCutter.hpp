// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkDataSetAlgorithm.hpp"
#include "quickVtkPlane.hpp"

#include <vtkSmartPointer.h>
#include <vtkCutter.h>
#include <vtkPlane.h>
#include <vtkPlaneCutter.h>

namespace quick
{

namespace Vtk
{

class PlaneCutter : public DataSetAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(Plane *plane READ getPlane WRITE setPlane)

public:
             PlaneCutter(void);
             PlaneCutter(vtkSmartPointer<vtkPlaneCutter>);
    virtual ~PlaneCutter(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkPlaneCutter>;

public:
    auto getPlane()
    {
        return m_plane;
    }

public:
    void setPlane(Plane *plane)
    {
        this->m_vtkObject->SetPlane(vtkPlane::SafeDownCast(plane->getVtkObject()));
        this->update();
    }

private:
    class PlaneCutterPrivate *d;

private:
    vtkSmartPointer<vtkPlaneCutter> m_vtkObject = nullptr;

    Plane *m_plane = nullptr;

private:
    static Qml::Register::Class<PlaneCutter> Register;
};

} // vtk

} // quick

//
// quickVtkPlaneCutter.hpp ends here
