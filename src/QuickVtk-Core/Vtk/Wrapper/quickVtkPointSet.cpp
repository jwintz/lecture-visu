// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkPointSet.hpp"

namespace quick {

namespace Vtk {

PointSet::PointSet(vtkSmartPointer<vtkPointSet> vtkObject) : DataSet(vtkObject)
{
    this->m_vtkObject = vtkPointSet::SafeDownCast(vtkObject);
}

PointSet::~PointSet(void)
{
    this->m_vtkObject = nullptr;
}

auto PointSet::getVtkObject(void) -> vtkSmartPointer<vtkPointSet>
{
    return this->m_vtkObject;
}

Qml::Register::AbstractClass<PointSet> PointSet::Register(true);

} // namespace Vtk

} // namespace quick

//
// quickVtkPointSet.cpp ends here
