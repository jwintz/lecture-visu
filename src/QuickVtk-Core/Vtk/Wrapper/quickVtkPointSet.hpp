// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore/QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkDataSet.hpp"

#include <vtkSmartPointer.h>
#include <vtkPointSet.h>

namespace quick {

namespace Vtk {

class PointSet : public DataSet
{
    Q_OBJECT

public:
             PointSet(void) = delete;
             PointSet(vtkSmartPointer<vtkPointSet>);
    virtual ~PointSet(void);

    auto getVtkObject(void) -> vtkSmartPointer<vtkPointSet>;

private:
    static Qml::Register::AbstractClass<PointSet> Register;

private:
    vtkSmartPointer<vtkPointSet> m_vtkObject = nullptr;
};

} // namespace Vtk

} // namespace quick

//
// quickVtkPointSet.hpp ends here
