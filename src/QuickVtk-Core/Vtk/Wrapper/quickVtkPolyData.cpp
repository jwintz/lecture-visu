// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkPolyData.hpp"

#include <vtkPoints.h>
#include <vtkCellArray.h>

namespace quick {

namespace Vtk {

class PolyDataPrivate
{
public:
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkCellArray> verts;
    vtkSmartPointer<vtkCellArray> lines;
    vtkSmartPointer<vtkCellArray> polys;
};

PolyData::PolyData(void) : PointSet(vtkSmartPointer<vtkPolyData>::New())
{
    d = new PolyDataPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    d->verts = vtkSmartPointer<vtkCellArray>::New();
    d->lines = vtkSmartPointer<vtkCellArray>::New();
    d->polys = vtkSmartPointer<vtkCellArray>::New();

    this->m_vtkObject = vtkPolyData::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);
    this->m_vtkObject->SetVerts(d->verts);
    this->m_vtkObject->SetLines(d->lines);
    this->m_vtkObject->SetPolys(d->polys);

    emit completed();
}

PolyData::PolyData(vtkSmartPointer<vtkPolyData> vtkObject) : PointSet(vtkObject)
{
    d = new PolyDataPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    d->verts = vtkSmartPointer<vtkCellArray>::New();
    d->lines = vtkSmartPointer<vtkCellArray>::New();
    d->polys = vtkSmartPointer<vtkCellArray>::New();

    this->m_vtkObject = vtkPolyData::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);
    this->m_vtkObject->SetVerts(d->verts);
    this->m_vtkObject->SetLines(d->lines);
    this->m_vtkObject->SetPolys(d->polys);

    emit completed();
}

auto PolyData::getVtkObject(void) -> vtkSmartPointer<vtkPolyData>
{
    return this->m_vtkObject;
}

quint32 PolyData::addPoint(qreal x, qreal y, qreal z)
{
    quint32 id = d->points->InsertNextPoint(x, y, z);

    d->points->Modified();

    m_vtkObject->Modified();

    return id;
}

quint32 PolyData::addVertex(quint32 id1)
{
    vtkIdType ids[1];
    ids[0] = id1;

    quint32 id = d->verts->InsertNextCell(1, &ids[0]);

    d->verts->Modified();

    m_vtkObject->Modified();

    this->update();

    return id;
}

quint32 PolyData::addLine(quint32 id1, quint32 id2)
{
    vtkIdType ids[2];
    ids[0] = id1;
    ids[1] = id2;

    quint32 id = d->lines->InsertNextCell(2, &ids[0]);

    d->lines->Modified();

    m_vtkObject->Modified();

    this->update();

    return id;
}

quint32 PolyData::addTriangle(quint32 id1, quint32 id2, quint32 id3)
{
    vtkIdType ids[3];
    ids[0] = id1;
    ids[1] = id2;
    ids[2] = id3;

    quint32 id = d->polys->InsertNextCell(3, &ids[0]);

    d->polys->Modified();

    m_vtkObject->Modified();

    this->update();

    return id;
}

PolyData::~PolyData(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<PolyData> PolyData::Register(true);

}

}

//
// quickVtkPolyData.cpp ends here
