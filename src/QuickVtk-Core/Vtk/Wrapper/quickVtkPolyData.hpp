// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkPointSet.hpp"

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>

namespace quick
{

namespace Vtk
{

class PolyData : public PointSet
{
    Q_OBJECT

public:
             PolyData(void);
             PolyData(vtkSmartPointer<vtkPolyData>);
    virtual ~PolyData(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkPolyData>;

public slots:
    quint32 addPoint(qreal, qreal, qreal);
    quint32 addVertex(quint32);
    quint32 addLine(quint32, quint32);
    quint32 addTriangle(quint32, quint32, quint32);

private:
    class PolyDataPrivate *d;

private:
    vtkSmartPointer<vtkPolyData> m_vtkObject = nullptr;

private:
    static Qml::Register::Class<PolyData> Register;
};

} // vtk

} // quick

//
// quickVtkPolyData.hpp ends here
