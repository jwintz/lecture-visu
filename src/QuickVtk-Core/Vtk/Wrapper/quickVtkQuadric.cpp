// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkQuadric.hpp"

#include <vtkPoints.h>
#include <vtkCellArray.h>

namespace quick {

namespace Vtk {

class QuadricPrivate
{
public:
};

Quadric::Quadric(void) : ImplicitFunction(vtkSmartPointer<vtkQuadric>::New())
{
    d = new QuadricPrivate;

    this->m_vtkObject = vtkQuadric::SafeDownCast(ImplicitFunction::getVtkObject());
}

Quadric::Quadric(vtkSmartPointer<vtkQuadric> vtkObject) : ImplicitFunction(vtkObject)
{
    d = new QuadricPrivate;

    this->m_vtkObject = vtkQuadric::SafeDownCast(ImplicitFunction::getVtkObject());
}

auto Quadric::getVtkObject(void) -> vtkSmartPointer<vtkQuadric>
{
    return this->m_vtkObject;
}

Quadric::~Quadric(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<Quadric> Quadric::Register(true);

}

}

//
// quickVtkQuadric.cpp ends here
