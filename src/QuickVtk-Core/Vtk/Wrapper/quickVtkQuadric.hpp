// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkImplicitFunction.hpp"

#include <vtkSmartPointer.h>
#include <vtkQuadric.h>

namespace quick
{

namespace Vtk
{

class Quadric : public ImplicitFunction
{
    Q_OBJECT

public:
             Quadric(void);
             Quadric(vtkSmartPointer<vtkQuadric>);
    virtual ~Quadric(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkQuadric>;

public slots:
    void setCoefficients(qreal a, qreal b, qreal c, qreal d, qreal e, qreal f, qreal g, qreal h, qreal i, qreal j)
    {
        this->m_vtkObject->SetCoefficients(a, b, c, d, e, f, g, h, i, j);
        this->update();
    }

private:
    class QuadricPrivate *d;

private:
    vtkSmartPointer<vtkQuadric> m_vtkObject = nullptr;

private:
    static Qml::Register::Class<Quadric> Register;
};

} // vtk

} // quick

//
// quickVtkQuadric.hpp ends here
