// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkSampleFunction.hpp"

namespace quick {

namespace Vtk {

class SampleFunctionPrivate
{
public:
};

SampleFunction::SampleFunction(void) : ImageAlgorithm(vtkSmartPointer<vtkSampleFunction>::New())
{
    d = new SampleFunctionPrivate;

    this->m_vtkObject = vtkSampleFunction::SafeDownCast(ImageAlgorithm::getVtkObject());

    this->m_sampleDimension = new Math::Vector3([this](){
        this->m_vtkObject->SetSampleDimensions(this->m_sampleDimension->getX(), this->m_sampleDimension->getY(), this->m_sampleDimension->getZ());
        this->update();
    });

}

SampleFunction::SampleFunction(vtkSmartPointer<vtkSampleFunction> vtkObject) : ImageAlgorithm(vtkObject)
{
    d = new SampleFunctionPrivate;

    this->m_vtkObject = vtkSampleFunction::SafeDownCast(ImageAlgorithm::getVtkObject());
}

auto SampleFunction::getVtkObject(void) -> vtkSmartPointer<vtkSampleFunction>
{
    return this->m_vtkObject;
}

    SampleFunction::~SampleFunction(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<SampleFunction> SampleFunction::Register(true);

}

}

//
// quickVtkSampleFunction.cpp ends here
