// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkImageAlgorithm.hpp"
#include "quickVtkImplicitFunction.hpp"
#include "quickMathVector3.hpp"

#include <vtkSmartPointer.h>
#include <vtkSampleFunction.h>

namespace quick
{

namespace Vtk
{

class SampleFunction : public ImageAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(quick::Math::Vector3* sampleDimension READ getSampleDimension CONSTANT);
    Q_PROPERTY(ImplicitFunction *implicitFunction READ getImplicitFunction WRITE setImplicitFunction)

public:
             SampleFunction(void);
             SampleFunction(vtkSmartPointer<vtkSampleFunction>);
    virtual ~SampleFunction(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkSampleFunction>;

public:
    auto getSampleDimension()
    {
        return m_sampleDimension;
    }

    auto getImplicitFunction(void)
    {
        return m_implicitFunction;
    }

public:
    void setImplicitFunction(ImplicitFunction *function)
    {
        this->m_vtkObject->SetImplicitFunction(function->getVtkObject());
        this->update();
    }

private:
    class SampleFunctionPrivate *d;

private:
    vtkSmartPointer<vtkSampleFunction> m_vtkObject = nullptr;

    ImplicitFunction *m_implicitFunction = nullptr;

private:
    Math::Vector3* m_sampleDimension = nullptr;
        
private:
    static Qml::Register::Class<SampleFunction> Register;
};

} // vtk

} // quick

//
// quickVtkSampleFunction.hpp ends here
