// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkStructuredGrid.hpp"

#include <vtkPoints.h>
#include <vtkCellArray.h>

namespace quick {

namespace Vtk {

class StructuredGridPrivate
{
public:
    vtkSmartPointer<vtkPoints> points;
};

StructuredGrid::StructuredGrid(void) : PointSet(vtkSmartPointer<vtkStructuredGrid>::New())
{
    d = new StructuredGridPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    this->m_vtkObject = vtkStructuredGrid::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);

    emit completed();
}

StructuredGrid::StructuredGrid(vtkSmartPointer<vtkStructuredGrid> vtkObject) : PointSet(vtkObject)
{
    d = new StructuredGridPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    this->m_vtkObject = vtkStructuredGrid::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);

    emit completed();
}

auto StructuredGrid::getVtkObject(void) -> vtkSmartPointer<vtkStructuredGrid>
{
    return this->m_vtkObject;
}

quint32 StructuredGrid::addPoint(qreal x, qreal y, qreal z)
{
    quint32 id = d->points->InsertNextPoint(x, y, z);

    d->points->Modified();

    m_vtkObject->Modified();

    return id;
}

StructuredGrid::~StructuredGrid(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<StructuredGrid> StructuredGrid::Register(true);

}

}

//
// quickVtkStructuredGrid.cpp ends here
