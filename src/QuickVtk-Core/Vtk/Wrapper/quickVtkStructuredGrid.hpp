// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkPointSet.hpp"

#include <vtkSmartPointer.h>
#include <vtkStructuredGrid.h>

namespace quick
{

namespace Vtk
{

class StructuredGrid : public PointSet
{
    Q_OBJECT

public:
             StructuredGrid(void);
             StructuredGrid(vtkSmartPointer<vtkStructuredGrid>);
    virtual ~StructuredGrid(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkStructuredGrid>;

public slots:
    quint32 addPoint(qreal, qreal, qreal);

private:
    class StructuredGridPrivate *d;

private:
    vtkSmartPointer<vtkStructuredGrid> m_vtkObject = nullptr;

private:
    static Qml::Register::Class<StructuredGrid> Register;
};

} // vtk

} // quick

//
// quickVtkStructuredGrid.hpp ends here
