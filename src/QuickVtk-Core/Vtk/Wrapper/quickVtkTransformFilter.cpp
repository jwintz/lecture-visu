// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkTransformFilter.hpp"

namespace quick {

namespace Vtk {

Qml::Register::Class<TransformFilter> TransformFilter::Register(true);

TransformFilter::TransformFilter() : PointSetAlgorithm(vtkSmartPointer<vtkTransformFilter>::New())
{
    this->m_transform = vtkSmartPointer<vtkTransform>::New();
    this->m_transform->Scale(1.0, 1.5, 2.0);

    this->m_vtkObject = vtkTransformFilter::SafeDownCast(this->getVtkObject());
    this->m_vtkObject->SetTransform(this->m_transform);

    this->m_scale = new Math::Vector3([this]()
    {

        // this->m_transform->Scale(this->m_scale->getValues().data());

        // this->m_vtkObject->SetTransform(this->m_transform);
       
        // this->update();
    });
}

auto TransformFilter::getScale(void) -> Math::Vector3*
{
    return m_scale;
}

}

}

//
// quickVtkTransformFilter.cpp ends here
