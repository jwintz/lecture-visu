// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "quickVtkPointSetAlgorithm.hpp"
#include "quickMathVector3.hpp"

#include <vtkTransform.h>
#include <vtkTransformFilter.h>

namespace quick {
    namespace Vtk {

        class TransformFilter : public PointSetAlgorithm {
            Q_OBJECT
            Q_PROPERTY(quick::Math::Vector3* scale READ getScale CONSTANT);

        private:
            static Qml::Register::Class<TransformFilter> Register;
            vtkSmartPointer<vtkTransformFilter> m_vtkObject = nullptr;
            vtkSmartPointer<vtkTransform> m_transform = nullptr;
            Math::Vector3* m_scale = nullptr;
        public:
            TransformFilter();
            auto getScale(void) -> Math::Vector3*;
        };
    }
}

//
// quickVtkTransformFilter.hpp ends here
