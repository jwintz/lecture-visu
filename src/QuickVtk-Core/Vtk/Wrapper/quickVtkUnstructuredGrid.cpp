// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "quickVtkUnstructuredGrid.hpp"

#include <vtkPoints.h>
#include <vtkCellArray.h>

namespace quick {

namespace Vtk {

class UnstructuredGridPrivate
{
public:
    vtkSmartPointer<vtkPoints> points;
};

UnstructuredGrid::UnstructuredGrid(void) : PointSet(vtkSmartPointer<vtkUnstructuredGrid>::New())
{
    d = new UnstructuredGridPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    this->m_vtkObject = vtkUnstructuredGrid::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);

    emit completed();
}

UnstructuredGrid::UnstructuredGrid(vtkSmartPointer<vtkUnstructuredGrid> vtkObject) : PointSet(vtkObject)
{
    d = new UnstructuredGridPrivate;

    d->points = vtkSmartPointer<vtkPoints>::New();

    this->m_vtkObject = vtkUnstructuredGrid::SafeDownCast(PointSet::getVtkObject());
    this->m_vtkObject->SetPoints(d->points);

    emit completed();
}

auto UnstructuredGrid::getVtkObject(void) -> vtkSmartPointer<vtkUnstructuredGrid>
{
    return this->m_vtkObject;
}

quint32 UnstructuredGrid::addPoint(qreal x, qreal y, qreal z)
{
    quint32 id = d->points->InsertNextPoint(x, y, z);

    d->points->Modified();

    m_vtkObject->Modified();

    return id;
}

UnstructuredGrid::~UnstructuredGrid(void)
{
    this->m_vtkObject = nullptr;
}

Qml::Register::Class<UnstructuredGrid> UnstructuredGrid::Register(true);

}

}

//
// quickVtkUnstructuredGrid.cpp ends here
