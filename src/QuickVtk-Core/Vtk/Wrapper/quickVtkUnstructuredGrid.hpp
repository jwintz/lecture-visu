// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QObject>

#include "quickQmlRegister.hpp"
#include "quickVtkPointSet.hpp"

#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

namespace quick
{

namespace Vtk
{

class UnstructuredGrid : public PointSet
{
    Q_OBJECT

public:
             UnstructuredGrid(void);
             UnstructuredGrid(vtkSmartPointer<vtkUnstructuredGrid>);
    virtual ~UnstructuredGrid(void);

public:
    auto getVtkObject(void) -> vtkSmartPointer<vtkUnstructuredGrid>;

public slots:
    quint32 addPoint(qreal, qreal, qreal);

private:
    class UnstructuredGridPrivate *d;

private:
    vtkSmartPointer<vtkUnstructuredGrid> m_vtkObject = nullptr;

private:
    static Qml::Register::Class<UnstructuredGrid> Register;
};

} // vtk

} // quick

//
// quickVtkUnstructuredGrid.hpp ends here
