#include "quickAppEngine.hpp"
#include "quickAppController.hpp"

#include "quickConfig.h"

#include "quickDocumentHighlighter.h"

#include "quickMenuController.hpp"
#include "quickSampleDataController.hpp"
#include "quickConsoleController.hpp"

#include "quickIO.hpp"
#include "meta_quickvtk.hpp"

#include "quickQmlRegister.hpp"

#include <QWindow>
#include <QFontDatabase>
#include <QApplication>
#include <QQmlContext>
#include <QQmlEngine>
#include <QDir>
#include <QQuickTextDocument>
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////////////////

template <class T> QList<T> childObjects(QQmlApplicationEngine *engine, const QString& objectName, const QString& propertyName)
{
    QList<QObject*> rootObjects = engine->rootObjects();

    QList<T> children;

    foreach (QObject* object, rootObjects) {

        foreach(QObject* child, object->findChildren<QObject*>(objectName)) {

            if (child != 0) {
                std::string s = propertyName.toStdString();
                QObject* object = child->property(s.c_str()).value<QObject*>();
                Q_ASSERT(object != 0);
                T prop = dynamic_cast<T>(object);
                Q_ASSERT(prop != 0);
                children << prop;
            }
        }
    }

    return children;
}

// /////////////////////////////////////////////////////////////////////////////
// Helper classes
// /////////////////////////////////////////////////////////////////////////////

class Helper : public QObject
{
    Q_OBJECT

public:
    static Helper *instance(void)
    {
        if(!s_instance)
            s_instance = new Helper;

        return s_instance;
    }

public slots:
    QString read(const QString& local)
    {
        QString path = QString(QUICKVTK_ROOT_PATH) + local;

        QFile file(path);
        file.open(QIODevice::ReadOnly);

        QString contents = file.readAll();

        file.close();

        return contents;
    }

private:
    static Helper *s_instance;

private:
     Helper(void) = default;
    ~Helper(void) = default;
};

Helper *Helper::s_instance = 0;

// /////////////////////////////////////////////////////////////////////////////

namespace quick {
    namespace App {
    
        Engine* Engine::instance = nullptr;

        auto Engine::HandleMessage(QtMsgType type, const QMessageLogContext& context, const QString& msg) -> void {

            // qDebug() << msg;

            if (instance->m_messageHandled.localData()) {
                return;
            }

            QScopedValueRollback<bool> roll(instance->m_messageHandled.localData(), true);
            
            switch(type) {
                // case QtMsgType::QtDebugMsg: Console::Controller::instance->addDebugMsg(msg); break;
                case QtMsgType::QtInfoMsg: Console::Controller::instance->addInfoMsg(msg); break;
                case QtMsgType::QtWarningMsg: Console::Controller::instance->addWarningMsg(msg); break;
                case QtMsgType::QtCriticalMsg: Console::Controller::instance->addErrorMsg(msg); break;
                default: {
                    break;
                }
            }
        }

        auto Engine::init() -> void {
            QApplication::setOrganizationName(Meta::OrgName);
            QApplication::setOrganizationDomain(Meta::OrgDomain);
            QApplication::setApplicationName(Meta::AppName);
            QApplication::setApplicationVersion(Meta::AppVersion);

            Qml::RegisterTypes();
            
            SampleData::Controller::Create();
            Menu::Controller::Init();

            auto path = QDir(QGuiApplication::applicationDirPath());

    #ifdef __APPLE__
            path.cdUp();
    #endif
            resourceDir = path.absolutePath() + "/Resources/";

            AddFontDir(resourceDir + "fonts/Fira_Code/");
            AddFontDir(resourceDir + "fonts/Roboto/");
            AddFontDir(resourceDir + "fonts/font-awesome/");

            auto engine = new QQmlApplicationEngine ();

            auto context = engine->rootContext();
            context->setContextProperty("App", Controller::instance);
            context->setContextProperty("SampleData", SampleData::Controller::GetInstance());
            context->setContextProperty("Helper", Helper::instance());

            engine->addImportPath(resourceDir + "qml");
            engine->load(QUrl::fromLocalFile(resourceDir + "qml/Deck/deck.qml"));

            auto rootObject = engine->rootObjects().at(0);
            auto window = static_cast<QWindow*>(rootObject);

// /////////////////////////////////////////////////////////////////////////////
// Setup highlighter
// /////////////////////////////////////////////////////////////////////////////

            foreach(QQuickTextDocument *document, childObjects<QQuickTextDocument *>(engine, "textEditor", "textDocument"))
                quickDocumentHighlighter *highlighter = new quickDocumentHighlighter(document->textDocument());

            // /////////////////////////////////////////////////////////////////

            QSurfaceFormat format;
            format.setMajorVersion(3);
            format.setMinorVersion(2);
            format.setDepthBufferSize(1);
            format.setStencilBufferSize(1);
            format.setProfile(QSurfaceFormat::CoreProfile);

            window->setFormat(format);
            window->resize(1024 * 1.5, 512 * 1.5); // TODO: Replace with HDPI scale factor
            window->show();
            window->raise();
        }

        auto Engine::Execute(int argc, char** argv) -> int
        {
            qInstallMessageHandler(HandleMessage);

            QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
            QApplication application(argc, argv);

            if (!instance) {
                instance = new Engine();
                instance->init();
                return application.exec();
            }

            return 1;
        }

        auto Engine::AddFontDir(const QString& directory) -> void {
            auto fontUrls = IO::FileUrlsFromDir(directory, {"*.ttf", "*.otf"}, IO::FileSuffix::On);

            for (const auto& fontUrl : fontUrls) {
                QFontDatabase::addApplicationFont(fontUrl);
            }
        }

        auto Engine::GetResourceDir() -> QString {
            return instance->resourceDir;
        }
    }
}

// /////////////////////////////////////////////////////////////////////////////

#include "quickAppEngine.moc"
