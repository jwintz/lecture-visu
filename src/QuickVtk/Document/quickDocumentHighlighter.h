// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>

class quickDocumentHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
     quickDocumentHighlighter(QTextDocument *parent = 0);
    ~quickDocumentHighlighter(void);

protected:
    void highlightBlock(const QString &text) override;

private:
    class quickDocumentHighlighterPrivate *d;
};

//
// quickDocumentHighlighter.h ends here
